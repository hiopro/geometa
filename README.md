## geometa
### 1. 项目简介
**Geometa** 是一个基于 ISO 标准的遥感元数据管理系统，可对符合 ISO 标准及自定义的遥感元数据进行存储与管理，提供对遥感元数据的入库、浏览、编辑、验证、下载、检索等功能。
目前支持元数据标准有：
* ISO 19163
  + CategoricalGriddedData：分类格网数据
  + OpticalImage：光学影像
  + SARData：雷达数据
  + RadiometerData：辐射计数据
  + FusedImage：融合图像
  + SimulatedImage：模拟图像
* ISO 19123
  + RectifiedGridCoverage: 校正格网覆盖
  + ReferenceableGridCoverage: 可参考格网数据

项目主要分为以下几个模块：
  * 元数据模块（metadata）：项目核心模块，对遥感元数据进行存储管理。
  * 用户管理模块（auth）：管理用户信息，发放登录凭证。
  * 数据验证模块（validate）：验证元数据是否符合 ISO 标准。
  * 网关模块（gateway）：统一各模块入口地址、统一鉴权。
  * 检索模块（search）：检索元数据信息（待完善）。

### 2. 项目部署
#### 2.1 运行环境
* MySQL >= 8.0
* Redis >= 3.0
* JDK >= 1.8

#### 2.1 项目运行
> 以下为 Linux 环境

下载项目源码。
```shell
$ git clone https://gitee.com/dalelee/geometa.git
```
使用 `sql/geometa.sql` 建立数据库，修改 `metadata` 和 `auth` 模块的数据库连接。

将项目打包。
```shell
$ mvn package -DskipTests
```
将 `auth`、`gateway`、`metadata`、`gateway`、`searth`（可选） 等 jar 文件和 `bin/start.sh` 拷贝到同一个文件夹下。
```text
.
├── geometa-auth-0.0.1-SNAPSHOT.jar
├── geometa-gateway-0.0.1-SNAPSHOT.jar
├── geometa-metadata-0.0.1-SNAPSHOT.jar
├── geometa-validate-0.0.1-SNAPSHOT.jar
├── start.sh
└── stop.sh
```
运行 `start.sh`。
```shell
$ sudo bash stop.sh 
```
访问登录页面（http://localhost:9090/metadata/login.html）。

<img src="static/img/login.png">

#### 2.3 停止运行
运行 `start.sh`。
```shell
$ sudo bash stop.sh 
```

### 3. 功能介绍
#### 3.1 数据入库
支持 URL、文件、文本三种入库方式。

<img src="static/img/import.png">

#### 3.2 数据管理
修改元数据的概要信息、批量删除与下载。

<img src="static/img/manage.png">

#### 3.3 命名空间管理
管理元数据的命名空间。

<img src="static/img/namespace.png">

#### 3.5 数据编辑
增加、删除、修改元数据节点。

<img src="static/img/edit.png">
<br/>
<img src="static/img/edit_curd.png">
<br/>
<img src="static/img/edit_info.png">

#### 3.6 数据校验
验证元数据是否符合 ISO 标准，生成测试报告。

<img src="static/img/validate_index.png">
<br/>
<img src="static/img/validate_report.png">