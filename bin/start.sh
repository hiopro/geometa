#!/bin/bash

source /etc/profile # 加载环境变量
prefix="geometa-"  # 指定需要启动的 Jar 包名称前缀
current_dir=$(dirname "$0") # 获取当前脚本所在目录
log_dir="${current_dir}/log"

# 判断 log 目录是否存在，如果不存在则创建
if [ ! -d "$log_dir" ]; then
  echo "Creating log directory..."
  mkdir "$log_dir"
fi

for jarfile in "$current_dir"/"$prefix"*.jar
do
  log_file="$log_dir/$(basename "$jarfile").log"
  nohup java -jar "$jarfile" > "$log_file" 2>&1 &
  echo "Started ${jarfile}."
  echo "Log file in: ${log_file}"
done

echo "Started all JARs."
