#!/bin/bash

prefix="geometa-"  # 指定需要停止的 Jar 包名称前缀

for pid in $(ps aux | grep "$prefix" | grep -v grep | awk '{print $2}')
do
  kill -9 "$pid"
  echo "Stopped $pid."
done

echo "Stopped all JARs."