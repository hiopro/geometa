package com.fzu.geometa.auth.comm;

/**
 * @www.codesheep.cn
 * 20190312
 */
public interface JwtConst {

    long EXPIRATION_TIME = 432_000_000;     // 5天(以毫秒ms计)
    String SECRET = "GeometaSecret";      // JWT密码
    String TOKEN_PREFIX = "Bearer";         // Token前缀
    String HEADER_STRING = "Authorization"; // 存放Token的Header Key
}
