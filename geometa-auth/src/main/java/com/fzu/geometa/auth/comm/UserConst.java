package com.fzu.geometa.auth.comm;

public interface UserConst {
    // 普通用户
    int ROLE_USER = 0;
    // 管理员
    int ROLE_ADMIN = 1;

    // 未激活
    int STATUS_NOT_ACTIVATED = 0;
    // 激活
    int STATUS_ACTIVATED = 1;
}
