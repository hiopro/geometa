package com.fzu.geometa.auth.config;

import com.google.code.kaptcha.Producer;
import com.google.code.kaptcha.impl.DefaultKaptcha;
import com.google.code.kaptcha.util.Config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Properties;

@Configuration
public class KaptchaConfig {
    @Bean
    public Producer kaptchaProducer() {
        //配置属性
        Properties properties = new Properties();
        properties.setProperty("kaptcha.image.width", "130");
        properties.setProperty("kaptcha.image.height", "48");
        properties.setProperty("kaptcha.textproducer.font.size", "32");
        properties.setProperty("kaptcha.textproducer.font.color", "0,0,0");
        //随机字符 数字+大小写字母
        StringBuilder builder = new StringBuilder();
        for (char c = '0'; c < '9'; c++) {
            builder.append(c);
        }
        for (char c = 'A'; c < 'Z'; c++) {
            builder.append(c);
        }
        for (char c = 'a'; c < 'z'; c++) {
            builder.append(c);
        }
        properties.setProperty("kaptcha.textproducer.char.string", builder.toString());
        //字符个数
        properties.setProperty("kaptcha.textproducer.char.length", "4");
        //不设置干扰
        properties.setProperty("kaptcha.noise.impl", "com.google.code.kaptcha.impl.NoNoise");

        DefaultKaptcha kaptcha = new DefaultKaptcha();
        Config config = new Config(properties);
        kaptcha.setConfig(config);
        return kaptcha;
    }
}
