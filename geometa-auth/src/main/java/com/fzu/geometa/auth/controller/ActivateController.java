package com.fzu.geometa.auth.controller;

import com.fzu.geometa.auth.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import static com.fzu.geometa.auth.comm.ActivationConst.ACTIVATION_REPEAT;
import static com.fzu.geometa.auth.comm.ActivationConst.ACTIVATION_SUCCESS;

@Controller
public class ActivateController {
    @Autowired
    private UserService userService;

    @GetMapping("/activation/{userId}/{code}")
    public String activation(
            Model model,
            @PathVariable int userId,
            @PathVariable String code) {
        int result = userService.activation(userId, code);
        if (result == ACTIVATION_SUCCESS) {
            model.addAttribute("msg", "激活成功，您的账号可以正常使用了。");
            model.addAttribute("target", "/login");
        } else if (result == ACTIVATION_REPEAT) {
            model.addAttribute("msg", "该账号已被激活，无需重复操作。");
            model.addAttribute("target", "/index");
        } else {
            model.addAttribute("msg", "激活失败，您的账号可以正常使用了。");
            model.addAttribute("target", "/index");
        }
        return "/site/operate-result";
    }
}
