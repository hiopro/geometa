package com.fzu.geometa.auth.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fzu.geometa.auth.model.po.User;


/**
 * @Entity com.nowcoder.community.entity.User
 */
public interface UserMapper extends BaseMapper<User> {

}




