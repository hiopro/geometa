package com.fzu.geometa.auth.model.dto;

import lombok.Data;

@Data
public class CaptchaValidation {
    String id;
    String value;
}
