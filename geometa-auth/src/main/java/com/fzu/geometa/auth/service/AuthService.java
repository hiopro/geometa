package com.fzu.geometa.auth.service;

import com.fzu.geometa.auth.model.po.User;

/**
 * @www.codesheep.cn
 * 20190312
 */
public interface AuthService {

    User register(User userToAdd);
    String login( String username, String password );
}
