package com.fzu.geometa.auth.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fzu.geometa.auth.model.LoginTicket;
import com.fzu.geometa.auth.model.po.User;

import java.util.Map;

/**
 *
 */
public interface UserService extends IService<User> {
    /**
     * 注册用户
     * @param user 用户信息
     * @return
     */
    @Deprecated
    Map<String, Object> register(User user);
    int activation(int userId, String code);
    @Deprecated
    Map<String, Object> login(String username, String password, int expiredSeconds);
    void logout(String ticket);
    LoginTicket findLoginTicket(String ticket);
    void updateHeader(Integer id, String headerUrl);
    boolean updatePassword(Integer id, String passWord);
    User findUserById(int id);
    User findUserByName(String username);

}
