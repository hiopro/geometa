package com.fzu.geometa.common.model;

import lombok.Data;

@Data
public class UserToken {
    Long id;
    String username;
    String role;
}
