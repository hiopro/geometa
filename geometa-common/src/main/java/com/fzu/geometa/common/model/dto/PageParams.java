package com.fzu.geometa.common.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class PageParams {

    //当前页码
    @ApiModelProperty("页码")
    private Long current;
    //每页显示记录数
    @ApiModelProperty("每页记录数")
    private Long size;

    public PageParams() {
    }

    public PageParams(Long current, Long size) {
        this.current = current;
        this.size = size;
    }
}
