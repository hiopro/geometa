package com.fzu.geometa.common.model.dto;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

@Data
@ToString
public class PageResult<E> extends RestResponse<List<E>> implements Serializable {

    //总记录数
    private Long total;

    //当前页码
    private Long current;

    //每页记录数
    private Long size;

    public PageResult(int code, String message, List<E> result, Long total, Long current, Long size) {
        super(code, message, result);
        this.total = total;
        this.current = current;
        this.size = size;
    }

    public PageResult(int code, String message, List<E> result, Long total) {
        super(code, message, result);
        this.total = total;
    }

    public static <E> PageResult<E>  simpleResult(List<E> result, Long total) {
        return new PageResult<>(SUCCESS,"success",result,total);
    }
}
