package com.fzu.geometa.common.model.dto;

import java.io.Serializable;

/**
 * @author Mr.Lee
 * @version 1.0
 * @description 和前端约定返回的异常信息模型
 * @date 2023/2/12 16:55
 */
public class RestErrorResponse extends RestResponse implements Serializable {

    public RestErrorResponse(String errMessage) {
        super(ERROR, errMessage);
        setMessage(errMessage);
    }

//    public String getErrMessage() {
//        return getMessage();
//    }
//
//    public void setErrMessage(String errMessage) {
//        setMessage(errMessage);
//    }
}
