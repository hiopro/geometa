package com.fzu.geometa.common.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author Mr.M
 * @version 1.0
 * @description 通用结果类型
 * @date 2022/9/13 14:44
 */

@Data
@NoArgsConstructor
@ToString
public class RestResponse<T> {

    public static final int SUCCESS = 0;
    public static final int ERROR = -1;

    /**
     * 响应编码,0为正常,-1错误
     */
    private Integer code;

    /**
     * 响应提示信息
     */
    private String message;

    /**
     * 响应内容
     */
    private T data;

    public RestResponse(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public RestResponse(int code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }


    public static <T> RestResponse<T> validFail() {
        return validFail("error");
    }

    public static <T> RestResponse<T> validFail(String msg) {
        return validFail(null, msg);
    }

    /**
     * 错误信息的封装
     *
     * @param msg
     * @param <T>
     * @return
     */
    public static <T> RestResponse<T> validFail(T result, String msg) {
        return new RestResponse<>(ERROR, msg, result);
    }


    /**
     * 添加正常响应数据（包含响应内容）
     *
     * @return RestResponse Rest服务封装相应数据
     */
//    public static <T> RestResponse<T> success(T result) {
//        return success(result, "success");
//    }

    public static <T> RestResponse<T> success(T result, String msg) {
        RestResponse<T> response = new RestResponse<>(SUCCESS, msg, result);
        return response;
    }

    public static <T> RestResponse<T> success(String message) {
        return success(null,message);
    }

    public static <T> RestResponse<T> success(T result) {
        return success(result,"success");
    }

    /**
     * 添加正常响应数据（不包含响应内容）
     *
     * @return RestResponse Rest服务封装相应数据
     */
    public static <T> RestResponse<T> success() {
        return success("success");
    }


    public Boolean isSucceed() {
        return this.code == SUCCESS;
    }

}