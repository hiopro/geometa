package com.fzu.geometa.metadata.common;

public interface Code {
    int SUCCEED = 0;
    int ERROR = 1;
}
