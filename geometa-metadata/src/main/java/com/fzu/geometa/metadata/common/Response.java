package com.fzu.geometa.metadata.common;

import lombok.Data;

/**
 * 通用返回类
 * @param <T> 返回的数据类型
 */
@Data
public class Response<T> {
    private int code;
    private String message;
    private T data;

    public Response(int code, String message, T data) {
        this.code = code;
        this.data = data;
        this.message = message;
    }

    public Response(Status status, T data) {
        this(status.getCode(), status.getMessage(), data);
    }
}
