package com.fzu.geometa.metadata.common;

import lombok.Data;

@Data
public class Status {
    int code;
    String message;

    public Status(int code, String message) {
        this.code = code;
        this.message = message;
    }
}
