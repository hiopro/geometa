package com.fzu.geometa.metadata.config;

import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.vividsolutions.jts.geom.Polygon;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JacksonConfig {

    // 全局配置 jackson 序列化方法
    // 将 Long 类型转为 String，避免 json 精度丢失
    // Polygon 直接使用 toString 方法
    @Bean
    public Jackson2ObjectMapperBuilderCustomizer customizer() {
        return builder -> builder.serializerByType(Long.class, new ToStringSerializer())
                .serializerByType(Polygon.class, new ToStringSerializer());
    }

}

