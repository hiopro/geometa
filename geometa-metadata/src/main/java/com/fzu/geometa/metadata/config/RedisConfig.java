package com.fzu.geometa.metadata.config;

import com.fzu.geometa.metadata.model.po.Register;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;

@Configuration
public class RedisConfig {

    @Bean
    public RedisTemplate<String, Register> redisTemplate(RedisConnectionFactory factory) {
        RedisTemplate<String, Register> template = new RedisTemplate<>();
        template.setConnectionFactory(factory);
        // 设置key的序列化方式=>string
        template.setKeySerializer(RedisSerializer.string());
        // 设置value的序列化方式=>json
        template.setValueSerializer(RedisSerializer.json());
        template.afterPropertiesSet();
        return template;
    }

}
