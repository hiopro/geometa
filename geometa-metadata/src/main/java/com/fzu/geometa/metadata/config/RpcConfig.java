package com.fzu.geometa.metadata.config;

import com.fzu.geometa.metadata.util.ValidateClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RpcConfig {
    @Value("${rpc.host}")
    private String host;
    @Value("${rpc.port}")
    private int port;

    @Bean
    public ValidateClient validateClient() {
        return new ValidateClient(host,port);
    }
}
