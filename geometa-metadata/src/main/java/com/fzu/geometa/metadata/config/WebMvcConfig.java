package com.fzu.geometa.metadata.config;

import com.fzu.geometa.metadata.controller.interceptor.LoginRequiredInterceptor;
import com.fzu.geometa.metadata.controller.interceptor.LoginTokenInterceptor;
import com.fzu.geometa.metadata.controller.interceptor.UpdateSearchInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebMvcConfig implements WebMvcConfigurer {
    @Autowired
    private LoginTokenInterceptor loginTokenInterceptor;
    @Autowired
    private LoginRequiredInterceptor loginRequiredInterceptor;
    @Autowired
    private UpdateSearchInterceptor updateSearchInterceptor;

    @Value("${enable-search}")
    private boolean enableSearch;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(loginTokenInterceptor)
                .excludePathPatterns("/**/*.css", "/**/*.js", "/**/*.png", "/**/*.jpg", "/**/*.jpeg");
        registry.addInterceptor(loginRequiredInterceptor)
                .excludePathPatterns("/**/*.css", "/**/*.js", "/**/*.png", "/**/*.jpg", "/**/*.jpeg");
        // 开启搜索更新
//        if (enableSearch) {
//            registry.addInterceptor(updateSearchInterceptor)
//                    .excludePathPatterns("/**/*.css", "/**/*.js", "/**/*.png", "/**/*.jpg", "/**/*.jpeg");
//        }

    }
}
