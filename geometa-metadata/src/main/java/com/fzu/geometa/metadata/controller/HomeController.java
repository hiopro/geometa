package com.fzu.geometa.metadata.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

    private ObjectMapper objectMapper = new ObjectMapper();

    @GetMapping("/index")
    public String index() {
        return "redirect:index.html";
    }

    @GetMapping("/test")
    public String text() {
        return "test";
    }

}
