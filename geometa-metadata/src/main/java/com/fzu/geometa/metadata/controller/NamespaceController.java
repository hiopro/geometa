package com.fzu.geometa.metadata.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fzu.geometa.common.model.dto.PageParams;
import com.fzu.geometa.common.model.dto.PageResult;
import com.fzu.geometa.common.model.dto.RestResponse;
import com.fzu.geometa.metadata.annotation.LoginRequired;
import com.fzu.geometa.metadata.model.dto.NamespaceAddRequest;
import com.fzu.geometa.metadata.model.dto.NamespaceUpdateRequest;
import com.fzu.geometa.metadata.model.po.Namespace;
import com.fzu.geometa.metadata.model.po.Register;
import com.fzu.geometa.metadata.service.NamespaceService;
import com.fzu.geometa.metadata.service.RegisterService;
import com.fzu.geometa.metadata.util.HostHolder;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Objects;

import static com.fzu.geometa.common.model.dto.PageResult.simpleResult;
import static com.fzu.geometa.common.model.dto.RestResponse.success;
import static com.fzu.geometa.common.model.dto.RestResponse.validFail;

@RestController
@RequestMapping("/namespace")
public class NamespaceController {
    @Autowired
    NamespaceService namespaceService;
    @Autowired
    RegisterService registerService;

    @Autowired
    HostHolder hostHolder;

    @GetMapping(path = "/list")
    @LoginRequired
    public PageResult<Namespace> list(PageParams request, String coverageId) {
        //Long cid = registerService.getCidByCoverageId(coverageId);
        Register register = registerService.getByCoverageId(coverageId);
        if (Objects.isNull(register)) {
            return simpleResult(null,0L);
        }
        // 必须是属于该用户的数据
        if (register.getUserId() != null && register.getUserId().equals(
                hostHolder.getUser().getId())) {
            return list(request, register.getCid());
        } else {
            return simpleResult(null,0L);
        }

    }

    @PostMapping(path = "/delete")
    public RestResponse delete(Long id) {
        if (namespaceService.safelyRemove(id)) {
            return success("删除成功");
        } else {
            return validFail("删除失败，命名空间可能正在使用");
        }
    }

    @PostMapping(path = "/add")
    public RestResponse add(NamespaceAddRequest request) {
        Namespace namespace = new Namespace();
        BeanUtils.copyProperties(request,namespace);
        if (namespaceService.safelyAdd(namespace)) {
            return success("添加成功");
        } else {
            return validFail("添加失败，命名空间可能已存在");
        }
    }

    @PostMapping(path = "/update")
    public RestResponse update(NamespaceUpdateRequest request) {
        Namespace namespace = new Namespace();
        BeanUtils.copyProperties(request,namespace);
        if (namespaceService.safelyUpdate(namespace)) {
            return success("更新成功");
        } else {
            return validFail("更新失败，命名空间可能正在使用");
        }
    }

    @GetMapping("/listByCid/")
    private PageResult<Namespace> list(PageParams request, Long cid) {
        // 执行分页查询
        Page<Namespace> page = new Page<>(request.getCurrent(), request.getSize());
        LambdaQueryWrapper<Namespace> lqw = new LambdaQueryWrapper<>();
        lqw.eq(Namespace::getCid,cid);
        page = namespaceService.page(page, lqw);
        // 获取分页信息
        long total = page.getTotal(); // 总记录数
        // 总页数
        List<Namespace> records = page.getRecords();// 当前页记录列表
        return simpleResult(records,total);
    }
}
