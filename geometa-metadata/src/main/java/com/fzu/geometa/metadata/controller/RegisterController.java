package com.fzu.geometa.metadata.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fzu.geometa.common.model.dto.PageParams;
import com.fzu.geometa.common.model.dto.PageResult;
import com.fzu.geometa.common.model.dto.RestResponse;
import com.fzu.geometa.metadata.annotation.LoginRequired;
import com.fzu.geometa.metadata.annotation.UpdateSearch;
import com.fzu.geometa.metadata.eventdriven.publisher.MetadataEventPublisher;
import com.fzu.geometa.metadata.model.dto.ChangeValidateRequest;
import com.fzu.geometa.metadata.model.dto.RegisterUpdateRequest;
import com.fzu.geometa.metadata.model.po.Register;
import com.fzu.geometa.metadata.service.RegisterService;
import com.fzu.geometa.metadata.model.dto.RegisterQueryRequest;
import com.fzu.geometa.metadata.util.HostHolder;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.fzu.geometa.common.model.dto.PageResult.simpleResult;
import static com.fzu.geometa.common.model.dto.RestResponse.*;
import static com.fzu.geometa.metadata.common.RegisterConst.VALIDATE_OFF;
import static com.fzu.geometa.metadata.common.RegisterConst.VALIDATE_ON;

@RestController
@RequestMapping("/register")
public class RegisterController {
    @Autowired
    RegisterService registerService;

    @Autowired
    HostHolder hostHolder;

    @Autowired
    MetadataEventPublisher eventPublisher;

    /**
     * 分页查询
     * @param request
     * @return
     */
    @GetMapping(path = "/list")
    @LoginRequired
    public PageResult<Register> list(PageParams request) {
        // 执行分页查询
        //Page<Register> page = registerService.page(new Page<>(request.getCurrent(),request.getSize()));
        Page<Register> page = registerService.pageByUserId(new Page<>(request.getCurrent(),request.getSize()),
                hostHolder.getUser().getId());
        // 获取分页信息
        long total = page.getTotal(); // 总记录数
        // 总页数
        List<Register> records = page.getRecords();// 当前页记录列表
        return simpleResult(records,total);
    }

    @GetMapping(path = "/{cid}")
    public Register getRegister(@PathVariable Long cid) {
        return registerService.getById(cid);
    }

    @Deprecated
    @PostMapping(path = "/delete")
    public RestResponse delete(Long cid) {
        if (cid == null) {
            return validFail();
        }
        if (registerService.removeById(cid)) {
            return success();
        } else {
            return validFail();
        }
    }

    @PostMapping(path = "/update")
    public RestResponse update(RegisterUpdateRequest request) {
        if (request == null || request.getCoverageId() == null) {
            return validFail();
        }
        Register register = new Register();
        BeanUtils.copyProperties(request,register);
        if (registerService.updateById(register)) {
            eventPublisher.publishUpdateEvent(register.getCid());
            return success();
        } else {
            return validFail();
        }
    }

    @GetMapping (path = "/query")
    @LoginRequired
    public PageResult<Register> query(PageParams pageParams,RegisterQueryRequest request) {
        if (request == null) {
            // 抛出通用异常
        }
        Page<Register> page = registerService.likePageQuery(pageParams,request,hostHolder.getUser().getId());
        return simpleResult(page.getRecords(),page.getTotal());
    }

    @PostMapping(path = "/changeValidate/")
    public RestResponse changeValidate(ChangeValidateRequest request) {
        if (request == null) {
            // 抛出异常
        }
        boolean res = false;
        if (request.getValidate() == VALIDATE_ON ||
                request.getValidate() == VALIDATE_OFF) {
            Register register = new Register();
            BeanUtils.copyProperties(request,register);
            res = registerService.updateById(register);
        }
        return res ? success("修改成功") : validFail("修改失败");
    }
}
