package com.fzu.geometa.metadata.controller;

import cn.hutool.core.util.ObjectUtil;
import com.fzu.geometa.common.model.dto.RestResponse;
import com.fzu.geometa.metadata.model.dto.SpatialInfoAddRequest;
import com.fzu.geometa.metadata.model.dto.SpatialQueryRequest;
import com.fzu.geometa.metadata.model.po.Register;
import com.fzu.geometa.metadata.model.po.SpatialInfo;
import com.fzu.geometa.metadata.service.RegisterService;
import com.fzu.geometa.metadata.service.SpatialInfoService;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.io.WKTReader;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

import static com.fzu.geometa.metadata.mapper.SpatialInfoMapper.RELATION_INTERSECTS;

@RestController
@RequestMapping("/spatial")
public class SpatialInfoController {
    @Resource
    private SpatialInfoService spatialInfoService;

    @Resource
    private RegisterService registerService;

    @GetMapping("/{id}")
    public RestResponse<SpatialInfo> getById(@PathVariable Long id) {
        if (ObjectUtil.isNull(id)) {
            return RestResponse.validFail("id 不能为空");
        }
        SpatialInfo spatialInfo = spatialInfoService.getById(id);
        return RestResponse.success(spatialInfo);
    }

    @GetMapping("/query")
    public RestResponse<List<SpatialInfo>> spatialQuery(SpatialQueryRequest request) {
        if (ObjectUtil.isNull(request)) {
            return RestResponse.validFail("请求数据为空");
        }
        if (ObjectUtil.isNull(request.getWkt())) {
            return RestResponse.validFail("WKT 为空");
        }

        if (ObjectUtil.isNull(request.getRelation())) {
            // 默认相交
            request.setRelation(RELATION_INTERSECTS);
        }
        WKTReader wktReader = new WKTReader();
        try {
            Geometry geometry = wktReader.read(request.getWkt());
            List<SpatialInfo> spatialInfoList = spatialInfoService.spatialInfoQuery((Polygon) geometry, request.getRelation());
            return RestResponse.success(spatialInfoList);
        } catch (Exception e) {
            return RestResponse.validFail(e.getMessage());
        }
    }

    @PostMapping("/add")
    public RestResponse add(SpatialInfoAddRequest request) {
        if (ObjectUtil.isNull(request)) {
            return RestResponse.validFail("请求数据为空");
        }

        WKTReader wktReader = new WKTReader();
        Polygon polygon = null;
        try {
            Geometry geometry = wktReader.read(request.getEnvelope());
            polygon = (Polygon) geometry;
        } catch (Exception e) {
            return RestResponse.validFail(e.getMessage());
        }

        Register register;
        // 优先 cid，cid 不存在再找 coverageId
        // 不对 cid, coverageId 一致性做验证
        if (ObjectUtil.isNotNull(request.getCid())) {
            register = registerService.getById(request.getCid());
        } else {
            register = registerService.getByCoverageId(request.getCoverageId());
        }

        if (ObjectUtil.isNull(register)) {
            return RestResponse.validFail("元数据不存在");
        }

        SpatialInfo spatialInfo = spatialInfoService.getByCid(register.getCid());
        if (ObjectUtil.isNull(spatialInfo)) {
            // 不存在，添加
            spatialInfo = new SpatialInfo();
            spatialInfo.setCid(register.getCid());
            spatialInfo.setEnvelope(polygon);
            spatialInfoService.save(spatialInfo);
        } else {
            // 存在，更新
            spatialInfo.setEnvelope(polygon);
            spatialInfoService.updateById(spatialInfo);
        }
        return RestResponse.success();
    }
}
