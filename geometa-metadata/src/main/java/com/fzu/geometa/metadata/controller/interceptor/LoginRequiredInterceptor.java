package com.fzu.geometa.metadata.controller.interceptor;

import com.fzu.geometa.metadata.annotation.LoginRequired;
import com.fzu.geometa.metadata.util.HostHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;

@Component
public class LoginRequiredInterceptor implements HandlerInterceptor {

    @Autowired
    private HostHolder hostHolder;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //判断拦截类型是不是方法
        if (handler instanceof HandlerMethod) {
            HandlerMethod handlerMethod = (HandlerMethod) handler;
            Method method = handlerMethod.getMethod();
            LoginRequired loginRequired = method.getAnnotation(LoginRequired.class);
            //判断是否有 LoginRequired 注解以及用户是否登录
            if (loginRequired != null && hostHolder.getUser() == null) {
                //重定向到登录页面
                response.sendRedirect(request.getContextPath() + "/login.html");
                return false;
            }
        }
        return true;
    }
}
