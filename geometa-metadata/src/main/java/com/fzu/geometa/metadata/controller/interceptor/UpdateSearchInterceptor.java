package com.fzu.geometa.metadata.controller.interceptor;

import com.fzu.geometa.metadata.annotation.UpdateSearch;
import com.fzu.geometa.metadata.model.po.Register;
import com.fzu.geometa.metadata.service.MetadataService;
import com.fzu.geometa.metadata.service.RegisterService;
import com.fzu.geometa.metadata.util.HostHolder;
import com.fzu.geometa.mq.EventProducer;
import com.fzu.geometa.mq.constant.EntityType;
import com.fzu.geometa.mq.constant.Topic;
import com.fzu.geometa.mq.model.Event;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.util.List;

@Deprecated
@Component
public class UpdateSearchInterceptor implements HandlerInterceptor {

    @Autowired
    HostHolder hostHolder;
    @Autowired
    EventProducer eventProducer;
    @Autowired
    RegisterService registerService;
    @Autowired
    MetadataService metadataService;
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        //判断拦截类型是不是方法
        if (handler instanceof HandlerMethod) {
            HandlerMethod handlerMethod = (HandlerMethod) handler;
            Method method = handlerMethod.getMethod();
            UpdateSearch updateSearch = method.getAnnotation(UpdateSearch.class);
            //判断是否有 UpdateSearch 注解以及是否有 cid
            if (updateSearch != null && hostHolder.getProcessedCid() != null ) {
                List<Long> cidList = hostHolder.getProcessedCid();
                Long userId = hostHolder.getUser().getId();
                for (Long cid : cidList) {
                    Register register = registerService.getById(cid);
                    Event event;
                    // 触发元数据发布事件
                    if (register != null) {
                        String content = metadataService.getMetadata(cid);
                        event = new Event()
                                .setTopic(Topic.PUBLISH)
                                .setUserId(userId)
                                .setEntityId(cid)
                                .setEntityType(EntityType.Metadata)
                                .setData("coverageId", register.getCoverageId())
                                .setData("description",register.getDescription())
                                .setData("createTime",register.getCreateTime())
                                .setData("content", content)
                                .setData("subtype",register.getSubtype());
                    } else {
                        // 删除元数据
                        event = new Event()
                                .setTopic(Topic.RETRACT)
                                .setUserId(userId)
                                .setEntityType(EntityType.Metadata)
                                .setEntityId(cid);
                    }
                    eventProducer.fireEvent(event);
                }
            }
        }
    }

}
