package com.fzu.geometa.metadata.eventdriven.event;

import org.springframework.context.ApplicationEvent;
import org.springframework.context.event.ApplicationContextEvent;

public class MetadataAddEvent extends ApplicationEvent {
    private Long cid;
    public MetadataAddEvent(Object source, Long cid) {
        super(source);
        this.cid = cid;
    }

    public Long getCid() {
        return cid;
    }
}
