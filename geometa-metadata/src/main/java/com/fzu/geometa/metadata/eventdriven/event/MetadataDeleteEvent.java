package com.fzu.geometa.metadata.eventdriven.event;

import cn.hutool.core.util.ObjectUtil;
import org.springframework.context.ApplicationEvent;

import java.util.ArrayList;
import java.util.List;

public class MetadataDeleteEvent extends ApplicationEvent {
    List<Long> deletedIds;
    public MetadataDeleteEvent(Object source) {
        super(source);
        deletedIds = new ArrayList<>();
    }

    public List<Long> getDeletedIds() {
        return deletedIds;
    }

    public void addId(Long id) {
        if (ObjectUtil.isNotNull(id)) {
            deletedIds.add(id);
        }
    }
}
