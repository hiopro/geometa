package com.fzu.geometa.metadata.eventdriven.event;

import org.springframework.context.ApplicationEvent;

public class MetadataUpdateEvent extends ApplicationEvent {
    Long cid;
    public MetadataUpdateEvent(Object source, Long cid) {
        super(source);
        this.cid = cid;
    }

    public Long getCid() {
        return cid;
    }
}
