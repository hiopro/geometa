package com.fzu.geometa.metadata.eventdriven.listener;

import com.fzu.geometa.metadata.eventdriven.event.MetadataDeleteEvent;
import com.fzu.geometa.metadata.service.MetadataService;
import com.fzu.geometa.metadata.service.RegisterService;
import com.fzu.geometa.metadata.util.HostHolder;
import com.fzu.geometa.mq.EventProducer;
import com.fzu.geometa.mq.constant.EntityType;
import com.fzu.geometa.mq.constant.Topic;
import com.fzu.geometa.mq.model.Event;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MetadataDeleteListener implements ApplicationListener<MetadataDeleteEvent> {
    @Autowired
    EventProducer eventProducer;
    @Autowired
    RegisterService registerService;
    @Autowired
    HostHolder hostHolder;

    @Override
    public void onApplicationEvent(MetadataDeleteEvent event) {
        List<Long> deletedIds = event.getDeletedIds();
        Long userId = hostHolder.getUser().getId();
        for (Long deletedId : deletedIds) {
            // 删除元数据
            Event retractEvent = new Event()
                    .setTopic(Topic.RETRACT)
                    .setUserId(userId)
                    .setEntityType(EntityType.Metadata)
                    .setEntityId(deletedId);
            eventProducer.fireEvent(retractEvent);
        }
    }
}
