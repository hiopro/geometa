package com.fzu.geometa.metadata.eventdriven.listener;

import com.fzu.geometa.metadata.eventdriven.event.MetadataAddEvent;
import com.fzu.geometa.metadata.eventdriven.event.MetadataUpdateEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class MetadataUpdateListener implements ApplicationListener<MetadataUpdateEvent> {
    @Autowired
    MetadataAddListener metadataAddListener;

    @Override
    public void onApplicationEvent(MetadataUpdateEvent event) {
        // 目前同元数据增加的逻辑一致
        metadataAddListener.onApplicationEvent(new MetadataAddEvent(event.getSource(),event.getCid()));
    }
}
