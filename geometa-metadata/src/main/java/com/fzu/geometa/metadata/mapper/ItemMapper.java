package com.fzu.geometa.metadata.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fzu.geometa.metadata.model.po.Item;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* @author DaleLee
* @description 针对表【item(元数据项表)】的数据库操作Mapper
* @createDate 2023-02-22 16:26:45
* @Entity pers.ldy.geometadata.domain.Item
*/
public interface ItemMapper extends BaseMapper<Item> {
    // 获取节点及其子节点
    List<Item> selectItemAndChildren(@Param("eid")Long eid);

    List<Item> selectByPid(@Param("pid") Long pid);

}




