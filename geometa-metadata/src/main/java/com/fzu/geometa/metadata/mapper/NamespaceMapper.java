package com.fzu.geometa.metadata.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fzu.geometa.metadata.model.po.Namespace;

/**
* @author DaleLee
* @description 针对表【namespace(命名空间表)】的数据库操作Mapper
* @createDate 2023-02-22 16:29:03
* @Entity pers.ldy.geometadata.domain.Namespace
*/
public interface NamespaceMapper extends BaseMapper<Namespace> {

}




