package com.fzu.geometa.metadata.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fzu.geometa.metadata.model.po.Register;

/**
* @author DaleLee
* @description 针对表【register(注册表)】的数据库操作Mapper
* @createDate 2023-03-13 19:03:57
* @Entity pers.ldy.geometadata.domain.Register
*/
public interface RegisterMapper extends BaseMapper<Register> {

}




