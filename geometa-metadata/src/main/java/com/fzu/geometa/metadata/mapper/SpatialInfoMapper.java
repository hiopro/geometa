package com.fzu.geometa.metadata.mapper;

import com.fzu.geometa.metadata.model.po.SpatialInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.vividsolutions.jts.geom.Polygon;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author DaleLee
 * @description 针对表【spatial_info】的数据库操作Mapper
 * @createDate 2023-06-24 16:07:50
 * @Entity com.fzu.geometa.metadata.model.po.SpatialInfo
 */
public interface SpatialInfoMapper extends BaseMapper<SpatialInfo> {
    String RELATION_CONTAINS = "Contains";
    String RELATION_INTERSECTS = "Intersects";
    String RELATION_WITHIN = "Within";

    // 重新实现 insert 方法，以支持 wkt 插入
    // Service 层的 save 方法也可重新使用了
    // 也可以不写这个方法，直接 SQL id 等于这个方法名
    int insert(SpatialInfo spatialInfo);

    /**
     * 空间查询
     *
     * @param polygon  传入的几何
     * @param relation 传入几何待查询几何的关系
     * @return
     */
    List<SpatialInfo> spatialInfoQuery(@Param("polygon") Polygon polygon, @Param("relation") String relation);

    int updateById(SpatialInfo spatialInfo);
}




