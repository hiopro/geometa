package com.fzu.geometa.metadata.model.dto;

import lombok.Data;

@Data
public class ChangeValidateRequest {
    Long cid;
    Integer validate;
}
