package com.fzu.geometa.metadata.model.dto;

import lombok.Data;

import java.util.List;

@Data
public class DataDownloadRequest {
    List<Long> ids;
}
