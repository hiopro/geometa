package com.fzu.geometa.metadata.model.dto;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class DataImportRequest {
    public static final String METHOD_URL = "url";
    public static final String METHOD_FILE = "file";
    public static final String METHOD_Text = "text";

    public String coverageId;
    public String subtype;
    public String description;
    public String standard;
    public Integer validate;
    public String method;
    public String url;
    public MultipartFile file;
    public String text;
}
