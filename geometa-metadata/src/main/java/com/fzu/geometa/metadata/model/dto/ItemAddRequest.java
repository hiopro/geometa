package com.fzu.geometa.metadata.model.dto;

import lombok.Data;

/**
 * item 添加请求
 */
@Data
public class ItemAddRequest {
    // 必须有 pid，否则不知道添加到哪
    // eid 由系统分配，不可指定
    // order 默认添加到最后一个，若指定了 order，不可越界
    private String name;

    private Long pid;

    private Integer order;

    private String namespace;

    private String attributes;

    private String value;

}
