package com.fzu.geometa.metadata.model.dto;

import lombok.Data;

/**
 * item 更新请求
 */
@Data
public class ItemUpdateRequest {

    // 必须有 eid，否则不知道更新那条数据
    // order 是不能被随便更新的，会破坏树结构
    // 且给 order 指定一个新值语义不明确，是与原值交换还是别的意思
    private Long eid;
    private String name;
    private String namespace;
    private String attributes;
    private String value;
}
