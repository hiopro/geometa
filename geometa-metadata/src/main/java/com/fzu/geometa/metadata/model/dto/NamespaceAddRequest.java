package com.fzu.geometa.metadata.model.dto;

import lombok.Data;

@Data
public class NamespaceAddRequest {
    // 不为空
    Long cid;
    // 不为空
    String prefix;
    //  不为空
    String uri;

}
