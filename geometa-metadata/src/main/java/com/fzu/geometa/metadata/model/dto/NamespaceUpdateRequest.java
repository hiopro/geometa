package com.fzu.geometa.metadata.model.dto;

import lombok.Data;

@Data
public class NamespaceUpdateRequest {
    // 非空
    private Long id;
    private String prefix;
    private String uri;
}
