package com.fzu.geometa.metadata.model.dto;

import lombok.Data;

@Data
public class RegisterQueryRequest{
    String coverageId;
    String subtype;
}
