package com.fzu.geometa.metadata.model.dto;

import lombok.Data;

@Data
public class RegisterUpdateRequest {
    // 必须有 cid
    private Long cid;
    private String coverageId;
    private String description;
    private String subtype;
    private String standard;
}
