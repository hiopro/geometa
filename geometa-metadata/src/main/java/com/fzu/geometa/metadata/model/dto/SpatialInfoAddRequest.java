package com.fzu.geometa.metadata.model.dto;

import lombok.Data;

@Data
public class SpatialInfoAddRequest {
    private String envelope;
    private Long cid;
    // 优先 cid，cid 不存在再找 coverageId
    private String coverageId;
}
