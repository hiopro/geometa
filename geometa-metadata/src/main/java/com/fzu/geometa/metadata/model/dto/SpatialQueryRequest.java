package com.fzu.geometa.metadata.model.dto;

import lombok.Data;

@Data
public class SpatialQueryRequest {
    private String wkt;
    private String relation;
}
