package com.fzu.geometa.metadata.model.po;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 元数据项表
 * @TableName item
 */
@TableName(value ="item")
@Data
public class Item implements Serializable {
    /**
     * 元数据项唯一标识
     */
    @TableId(value = "eid")
    private Long eid;

    /**
     * 元数据项名称
     */
    @TableField(value = "name")
    private String name;

    /**
     * 父元数据项标识
     */
    @TableField(value = "pid")
    private Long pid;

    /**
     * 在父元数据项中出现的次序
     */
    @TableField(value = "`order`")
    private Integer order;

    /**
     * 命名空间
     */
    // 同下
    @TableField(value = "namespace",updateStrategy = FieldStrategy.IGNORED)
    private String namespace;

    /**
     * 属性
     */
    // 设置为 null 时也会更新数据库，将该字段在数据库中设为 null
    // mp 在一个字段为 null 时，会在 update 语句中移除该字段
    @TableField(value = "attributes",updateStrategy = FieldStrategy.IGNORED)
    private String attributes;

    /**
     * 元数据项的值
     */
    // 同上
    @TableField(value = "value",updateStrategy = FieldStrategy.IGNORED)
    private String value;

    /**
     * 所属的 Coverage
     */
    @TableField(value = "cid")
    private Long cid;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}