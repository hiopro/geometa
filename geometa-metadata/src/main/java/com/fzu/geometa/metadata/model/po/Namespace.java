package com.fzu.geometa.metadata.model.po;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 命名空间表
 * @TableName namespace
 */
@TableName(value ="namespace")
@Data
public class Namespace implements Serializable {
    /**
     * 唯一标识
     */
    @TableId(value = "id")
    private Long id;

    /**
     * 前缀名称
     */
    @TableField(value = "prefix")
    private String prefix;

    /**
     * uri标识
     */
    @TableField(value = "uri")
    private String uri;

    /**
     * 所属的coverage
     */
    @TableField(value = "cid")
    private Long cid;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}