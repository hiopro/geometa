package com.fzu.geometa.metadata.model.po;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 注册表
 * @TableName register
 */
@TableName(value ="register")
@Data
public class Register implements Serializable {
    /**
     * Coverage 唯一标识
     */
    @TableId
    private Long cid;

    /**
     * 字符形式的 Coverage 唯一标识
     */
    private String coverageId;

    /**
     * 描述信息
     */
    private String description;

    /**
     * 子类型
     */
    private String subtype;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 创建者
     */
    private Long userId;

    /**
     * 元数据标准
     */
    private String standard;

    /**
     * 是否开启验证 0 关闭 1 开启
     */
    private Integer validate;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}