package com.fzu.geometa.metadata.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializerBase;
import com.fzu.geometa.metadata.mapper.handler.PolygonTypeHandler;
import com.vividsolutions.jts.geom.Polygon;
import lombok.Data;

/**
 * 
 * @TableName spatial_info
 */
@TableName(value ="spatial_info")
@Data
public class SpatialInfo implements Serializable {
    /**
     * id
     */
    @TableId
    private Long id;

    /**
     * 空间范围 (srid 统一规定为 4326)
     */
    @TableField(typeHandler = PolygonTypeHandler.class)
    private Polygon envelope;

    /**
     * 元数据 id
     */
    private Long cid;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}