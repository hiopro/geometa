package com.fzu.geometa.metadata.model.vo;

import com.fzu.geometa.metadata.model.po.Item;
import lombok.Data;

@Data
public class TreeNode {
    private String title;
    private String id;
    private String parentId;

    public TreeNode(Item item) {
        this.id = item.getEid().toString();
        this.parentId = item.getPid().toString();
        if (item.getNamespace() != null) {
            this.title = "[" + item.getNamespace() + "] " + item.getName();
        } else {
            this.title = item.getName();
        }
    }
}
