package com.fzu.geometa.metadata.service;

@Deprecated
public interface CacheService {
    // 放入元数据
    void put(String coverageId, String content);
    // 取出元数据
    String get(String coverageId);
    // 删除元数据
    void remove(String coverageId);
}
