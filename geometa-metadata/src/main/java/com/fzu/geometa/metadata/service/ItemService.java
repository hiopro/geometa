package com.fzu.geometa.metadata.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fzu.geometa.metadata.model.po.Item;

import java.util.List;

/**
* @author DaleLee
* @description 针对表【item(元数据项表)】的数据库操作Service
* @createDate 2023-02-22 16:26:45
*/
public interface ItemService extends IService<Item> {
    List<Item> listByCid(Long cid);
    boolean removeByCid(Long cid);
    List<Item> listByPid(Long pid);

    // 删除节点及其子节点
    boolean removeItemAndChildren(Long eid);

    //  添加一个节点并调整树
    boolean addItemAndAdjustTree(Item item);

    // 批量删除
    boolean removeBatchByCid(List<Long> ids);
}
