package com.fzu.geometa.metadata.service;

import com.fzu.geometa.metadata.model.po.Item;

public interface ItemValidateService {
    boolean updateWithValidation(Item item);

    boolean addWithValidation(Item item);

    boolean deleteWithValidation(Long eid);

}
