package com.fzu.geometa.metadata.service;

import com.fzu.geometa.metadata.model.po.Register;

import java.io.InputStream;
import java.net.URL;
import java.util.List;

public interface MetadataService {
    // 通过字符串文本注册元数据
    boolean registerMetadata(Register register, String content);
    // 通过 url 注册元数据
    boolean registerMetadata(Register register, URL url);
    // 通过输入流注册元数据
    boolean registerMetadata(Register register, InputStream inputStream);
    String getMetadata(String coverageId);
    boolean deleteMetaData(String coverageId);

    boolean deleteMetaData(Long cid);

    String getMetadata(Long cid);

    // 节点操作
    // 查询
    String query(String coverageId,String nodePath);
    boolean del(String coverageId,String nodePath);
    boolean add(String coverageId,String nodePath,String content,Integer seq);

    // 批量删除元数据
    boolean removeBatch(List<Long> ids);
}
