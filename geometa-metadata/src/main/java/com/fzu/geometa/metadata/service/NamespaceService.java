package com.fzu.geometa.metadata.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fzu.geometa.metadata.model.po.Namespace;

import java.util.List;

/**
* @author DaleLee
* @description 针对表【namespace(命名空间表)】的数据库操作Service
* @createDate 2023-02-22 16:29:03
*/
public interface NamespaceService extends IService<Namespace> {
    List<Namespace> listByCid(Long cid);
    boolean removeByCid(Long cid);

    // 批量删除
    boolean removeBatchByCid(List<Long> ids);

    // 安全地删除一个命名空间，如果命名空间正在使用则删除失败
    boolean safelyRemove(Long id);

    // 安全地修改一个命名空间，如果命名空间正在使用则修改失败
    boolean safelyUpdate(Namespace namespace);

    // 安全地增加一个命名空间，如果命名空间已存在则增加失败
    boolean safelyAdd(Namespace namespace);

    // 判断一个命名空间是否合法
    boolean isValidNamespace(Long cid, String namespace);
}
