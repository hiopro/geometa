package com.fzu.geometa.metadata.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.fzu.geometa.common.model.dto.PageParams;
import com.fzu.geometa.metadata.model.dto.RegisterQueryRequest;
import com.fzu.geometa.metadata.model.po.Register;

/**
* @author DaleLee
* @description 针对表【register(注册表)】的数据库操作Service
* @createDate 2023-03-13 19:03:57
*/
public interface RegisterService extends IService<Register> {
        String getCoverageIdByCid(Long cid);
        Long getCidByCoverageId(String coverageId);
        Register getByCoverageId(String coverageId);
        Page<Register> likePageQuery(PageParams pageParams, RegisterQueryRequest request);
        Page<Register> likePageQuery(PageParams pageParams, RegisterQueryRequest request, Long userId);
        Page<Register> pageByUserId(Page<Register> page, Long userId);
}
