package com.fzu.geometa.metadata.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fzu.geometa.metadata.model.po.SpatialInfo;
import com.vividsolutions.jts.geom.Polygon;

import java.util.List;

/**
* @author DaleLee
* @description 针对表【spatial_info】的数据库操作Service
* @createDate 2023-06-24 16:07:50
*/
public interface SpatialInfoService extends IService<SpatialInfo> {
    List<SpatialInfo> spatialInfoQuery(Polygon polygon, String relation);

    SpatialInfo getByCid(Long cid);
}
