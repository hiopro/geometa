package com.fzu.geometa.metadata.service;

import com.fzu.geometa.metadata.model.vo.TreeNode;

import java.util.List;

public interface TreeNodeService {
    List<TreeNode> listByCid(Long cid);
    List<TreeNode> listByCoverageId(String coverageId);
}
