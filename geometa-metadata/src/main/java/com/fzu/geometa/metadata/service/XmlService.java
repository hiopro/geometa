package com.fzu.geometa.metadata.service;

import com.fzu.geometa.metadata.model.po.Item;
import com.fzu.geometa.metadata.model.po.Namespace;
import lombok.Data;
import org.dom4j.Document;
import org.dom4j.Element;
import org.xml.sax.InputSource;

import java.io.InputStream;
import java.io.Reader;
import java.net.URL;
import java.util.List;
import java.util.Map;

public interface XmlService {
    @Data
    class ItemsAndNs{
        private List<Item> items;
        private List<Namespace> namespaces;
    }
    List<Namespace> getNamespaces(String xmlStr);
    List<Item> getItems(String xmlStr);

    ItemsAndNs getItemsAndNs(String xmlStr);
    ItemsAndNs getItemsAndNs(URL url);
    ItemsAndNs getItemsAndNs(InputStream is);
    List<Namespace> getNamespaces(InputStream is);
    List<Item> getItems(InputStream is);

    List<Namespace> getNamespaces(Reader reader);
    List<Item> getItems(Reader reader);

    List<Namespace> getNamespaces(InputSource is);
    List<Item> getItems(InputSource is);

    String getXmlString(List<Item> items, List<Namespace> namespaces);

    // 获得文档以及 元素的 id map
    Document getDocAndIdMap(List<Item> items, List<Namespace> namespaces, Map<Element,Long> map);

    Element xmlStrToElement(String xmlStr);
}
