package com.fzu.geometa.metadata.service.impl;

import cn.hutool.cache.Cache;
import cn.hutool.cache.CacheUtil;
import com.fzu.geometa.metadata.service.CacheService;
import org.springframework.stereotype.Service;

@Service
public class CacheServiceImpl implements CacheService {
    private final Cache<String, String> lruCache = CacheUtil.newLRUCache(128);

    @Override
    public void put(String coverageId, String content) {
        lruCache.put(coverageId,content);
    }

    @Override
    public String get(String coverageId) {
        return lruCache.get(coverageId);
    }

    @Override
    public void remove(String coverageId) {
        lruCache.remove(coverageId);
    }
}
