package com.fzu.geometa.metadata.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fzu.geometa.metadata.model.po.Item;
import com.fzu.geometa.metadata.mapper.ItemMapper;
import com.fzu.geometa.metadata.model.po.Register;
import com.fzu.geometa.metadata.service.ItemService;
import com.fzu.geometa.metadata.service.MetadataService;
import com.fzu.geometa.metadata.service.RegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.fzu.geometa.metadata.common.RegisterConst.VALIDATE_ON;

/**
* @author DaleLee
* @description 针对表【item(元数据项表)】的数据库操作Service实现
* @createDate 2023-02-22 16:26:45
*/
@Service
@Transactional
public class ItemServiceImpl extends ServiceImpl<ItemMapper, Item>
    implements ItemService{

    @Override
    public List<Item> listByCid(Long cid) {
        LambdaQueryWrapper<Item> itemLQW = new LambdaQueryWrapper<>();
        itemLQW.eq(Item::getCid, cid);
        return this.list(itemLQW);
    }

    @Override
    public boolean removeByCid(Long cid) {
        LambdaQueryWrapper<Item> itemLQW = new LambdaQueryWrapper<>();
        itemLQW.eq(Item::getCid, cid);
        return this.remove(itemLQW);
    }

    @Override
    public List<Item> listByPid(Long pid) {
        LambdaQueryWrapper<Item> lqw = new LambdaQueryWrapper<>();
        lqw.eq(Item::getPid, pid);
        return this.list(lqw);
    }

    @Override
    public boolean removeItemAndChildren(Long eid) {
        // 1. 查询待删除的节点及其子节点
        List<Item> items = baseMapper.selectItemAndChildren(eid);
        Item root = null;
        List<Long> delIds = new ArrayList<>();
        for (Item item : items) {
            if (item.getEid().equals(eid)) {
                root = item;
            }
            delIds.add(item.getEid());
        }
        if (root == null) {
            return false;
        }
        // 2. 查询兄弟节点
        Long pid = root.getPid();
        Integer order = root.getOrder();
        List<Item> brothers = baseMapper.selectByPid(pid);
        // 3. 修改 order 并提交
        updateBatchById(brothers
                .stream()
                .filter(i->i.getOrder() > order)
                .peek(i->i.setOrder(i.getOrder() - 1))
                .collect(Collectors.toList()));
        // 4.执行删除
        this.removeBatchByIds(delIds);
        return true;
    }

    @Override
    public boolean addItemAndAdjustTree(Item item) {
        // 必须有父节点
        if (item.getPid() == null) {
            return false;
        }

        // 1. 查询父节点，设置所属的数据集
        Item parent = getById(item.getPid());
        if (parent == null) {
            return false;
        }

        // 和父节点设为同一数据集
        item.setCid(parent.getCid());

        // 2. 查询兄弟节点并核对 order 是否正确
        List<Item> brothers = listByPid(item.getPid());

        if (item.getOrder() == null) {
            // 默认插到最后 (order 从 0 开始)
            item.setOrder(brothers.size());
        } else if (item.getOrder() > brothers.size() || item.getOrder() < 0) {
            // 越界了
            return false;
        }

        // 3. 修改兄弟节点的 order，并将被修改的节点保存到数据库
        updateBatchById(brothers
                .stream()
                .filter(i->i.getOrder() >= item.getOrder())
                .peek(i->i.setOrder(i.getOrder() + 1))
                .collect(Collectors.toList()));

        // 4. 提交节点
        save(item);
        return true;
    }

    @Override
    public boolean removeBatchByCid(List<Long> ids) {
        LambdaUpdateWrapper<Item> luw = new LambdaUpdateWrapper<>();
        luw.in(Item::getCid,ids);
        return remove(luw);
    }
}




