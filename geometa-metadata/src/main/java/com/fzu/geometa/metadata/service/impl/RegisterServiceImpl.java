package com.fzu.geometa.metadata.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fzu.geometa.common.model.dto.PageParams;
import com.fzu.geometa.metadata.mapper.RegisterMapper;
import com.fzu.geometa.metadata.model.po.Register;
import com.fzu.geometa.metadata.service.RegisterService;
import com.fzu.geometa.metadata.model.dto.RegisterQueryRequest;
import com.fzu.geometa.metadata.util.RedisKeyUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;

/**
* @author DaleLee
* @description 针对表【register(注册表)】的数据库操作Service实现
* @createDate 2023-03-13 19:03:57
*/
@Service
public class RegisterServiceImpl extends ServiceImpl<RegisterMapper, Register>
    implements RegisterService {

    @Autowired
    private RedisTemplate<String,Register> redisTemplate;

    @Override
    public Register getById(Serializable id) {
        // 先找缓存
        String key = RedisKeyUtils.getRegisterKey((Long) id);
        ValueOperations<String, Register> ops = redisTemplate.opsForValue();
        Register register = ops.get(key);
        // 缓存存在
        if (ObjectUtil.isNotNull(register)) {
            return register;
        }

        // 缓存不存在，查数据库
        register = super.getById(id);
        // 放入缓存，24 小时
        ops.set(key,register,24, TimeUnit.HOURS);
        return register;
    }

    @Override
    public boolean removeById(Serializable id) {
        // 更新 dp
        boolean succeed = super.removeById(id);
        // 缓存失效
        if (succeed) {
            succeed = Boolean.TRUE.equals(redisTemplate.delete(RedisKeyUtils.getRegisterKey((Long) id)));
        }
        return succeed;
    }

    @Override
    public boolean updateById(Register entity) {
        if (ObjectUtil.isNull(entity) || ObjectUtil.isNull(entity.getCid())) {
            return false;
        }
        // 更新 db
        boolean succeed = super.updateById(entity);
        // 缓存失效
        if (succeed) {
            succeed = Boolean.TRUE.equals(redisTemplate.delete(RedisKeyUtils.getRegisterKey(entity.getCid())));

        }
        return succeed;
    }

    @Override
    public String getCoverageIdByCid(Long cid) {
        Register register = this.getById(cid);
        return register == null ? null : register.getCoverageId();
    }

    @Override
    public Long getCidByCoverageId(String coverageId) {
        Register register = getByCoverageId(coverageId);
        return register == null ? null : register.getCid();
    }

    @Override
    public Register getByCoverageId(String coverageId) {
        LambdaQueryWrapper<Register> lqw = new LambdaQueryWrapper<>();
        lqw.eq(Register::getCoverageId,coverageId);
        return this.getOne(lqw);
    }

    @Override
    public Page<Register> likePageQuery(PageParams pageParams,RegisterQueryRequest request) {
        LambdaQueryWrapper<Register> lqw = new LambdaQueryWrapper<>();
        if (!StringUtils.isBlank(request.getCoverageId())) {
            lqw.like(Register::getCoverageId,request.getCoverageId());
        }

        if (!StringUtils.isBlank(request.getSubtype())) {
            lqw.like(Register::getSubtype,request.getSubtype());
        }
        Page<Register> page = new Page<>(pageParams.getCurrent(), pageParams.getSize());
        return page(page,lqw);
    }

    @Override
    public Page<Register> likePageQuery(PageParams pageParams, RegisterQueryRequest request, Long userId) {
        LambdaQueryWrapper<Register> lqw = new LambdaQueryWrapper<>();
        lqw.eq(Register::getUserId,userId);

        if (!StringUtils.isBlank(request.getCoverageId())) {
            lqw.like(Register::getCoverageId,request.getCoverageId());
        }

        if (!StringUtils.isBlank(request.getSubtype())) {
            lqw.like(Register::getSubtype,request.getSubtype());
        }

        Page<Register> page = new Page<>(pageParams.getCurrent(), pageParams.getSize());
        return page(page,lqw);
    }

    @Override
    public Page<Register> pageByUserId(Page<Register> page, Long userId) {
        LambdaQueryWrapper<Register> lqw = new LambdaQueryWrapper<>();
        lqw.eq(Register::getUserId,userId);
        return page(page,lqw);
    }
}




