package com.fzu.geometa.metadata.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fzu.geometa.metadata.model.po.SpatialInfo;
import com.fzu.geometa.metadata.service.SpatialInfoService;
import com.fzu.geometa.metadata.mapper.SpatialInfoMapper;
import com.vividsolutions.jts.geom.Polygon;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

import static com.fzu.geometa.metadata.mapper.SpatialInfoMapper.*;

/**
* @author DaleLee
* @description 针对表【spatial_info】的数据库操作Service实现
* @createDate 2023-06-24 16:07:50
*/
@Service
public class SpatialInfoServiceImpl extends ServiceImpl<SpatialInfoMapper, SpatialInfo>
    implements SpatialInfoService{

    @Override
    public List<SpatialInfo> spatialInfoQuery(Polygon polygon, String relation) {
        // 忽略大小写
        if (StrUtil.equalsIgnoreCase(RELATION_INTERSECTS,relation)) {
            relation = RELATION_INTERSECTS;
        } else if (StrUtil.equalsIgnoreCase(RELATION_CONTAINS,relation)) {
            relation = RELATION_CONTAINS;
        } else if (StrUtil.equalsIgnoreCase(RELATION_WITHIN,relation)) {
            relation = RELATION_WITHIN;
        } else {
            return Collections.emptyList();
        }
        return this.baseMapper.spatialInfoQuery(polygon, relation);
    }

    @Override
    public SpatialInfo getByCid(Long cid) {
        LambdaQueryWrapper<SpatialInfo> lqw = new LambdaQueryWrapper<>();
        lqw.eq(SpatialInfo::getCid,cid);
        return this.getOne(lqw);
    }
}




