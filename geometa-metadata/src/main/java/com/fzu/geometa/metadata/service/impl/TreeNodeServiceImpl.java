package com.fzu.geometa.metadata.service.impl;

import com.fzu.geometa.metadata.model.po.Item;
import com.fzu.geometa.metadata.model.vo.TreeNode;
import com.fzu.geometa.metadata.service.RegisterService;
import com.fzu.geometa.metadata.service.TreeNodeService;
import com.fzu.geometa.metadata.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TreeNodeServiceImpl implements TreeNodeService {
    @Autowired
    private ItemService itemService;

    @Autowired
    private RegisterService registerService;

    @Override
    public List<TreeNode> listByCid(Long cid) {
        List<Item> list = itemService.listByCid(cid);
        return list.stream()
                .sorted(Comparator.comparing(Item::getOrder))
                .map(TreeNode::new)
                .collect(Collectors.toList());
    }

    @Override
    public List<TreeNode> listByCoverageId(String coverageId) {
        Long cid = registerService.getCidByCoverageId(coverageId);
        return listByCid(cid);
    }
}
