package com.fzu.geometa.metadata.util;

import com.fzu.geometa.common.model.UserToken;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * 持有用户信息,用于代替session对象.
 */
@Component
public class HostHolder {

    private ThreadLocal<UserToken> users = new ThreadLocal<>();
    private ThreadLocal<List<Long>> cidList = new ThreadLocal<>();

    public void setUser(UserToken user) {
        users.set(user);
    }

    public UserToken getUser() {
        return users.get();
    }

    @Deprecated
    public List<Long> getProcessedCid() {
        return cidList.get();
    }

    @Deprecated
    public void addProcessedCid(Long cid) {
        if (cidList.get() == null) {
            cidList.set(new ArrayList<>());
        }
        this.cidList.get().add(cid);
    }

    public void clear() {
        users.remove();
        cidList.remove();
    }
}