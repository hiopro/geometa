package com.fzu.geometa.metadata.util;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.fzu.geometa.common.exception.GeometaException;

public class RedisKeyUtils {
    // 分割符
    private static final String SPLIT = ":";
    // 元数据前缀
    private static final String PREFIX_METADATA = "geomate:metadata";

    private static final String PREFIX_REGISTER = "geomate:register";

    public static String getMetadataKey(String coverageId) {
        if (StrUtil.isBlank(coverageId)) {
            throw new GeometaException("CoverageId 不能为空.");
        }
        return PREFIX_METADATA + SPLIT + coverageId;
    }

    public static String getRegisterKey(Long cid) {
        if (ObjectUtil.isNull(cid)) {
            throw new GeometaException("cid 不能为空");
        }
        return PREFIX_REGISTER + SPLIT + cid;
    }
}
