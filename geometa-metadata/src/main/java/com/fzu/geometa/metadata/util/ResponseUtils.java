package com.fzu.geometa.metadata.util;

import com.fzu.geometa.metadata.common.Code;
import com.fzu.geometa.metadata.common.Response;
import com.fzu.geometa.metadata.common.Status;

public class ResponseUtils {
    public static final Status SUCCEED = new Status(Code.SUCCEED,"succeed");
    public static final Status FAILED = new Status(Code.ERROR,"failed");

    public static<T> Response<T> success(T data) {
        return new Response<>(Code.SUCCEED,"OK",data);
    }

    public static Status error(String message) {
        return new Status(Code.ERROR,message);
    }
}
