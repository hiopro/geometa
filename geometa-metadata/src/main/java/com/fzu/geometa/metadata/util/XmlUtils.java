package com.fzu.geometa.metadata.util;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

import java.io.IOException;
import java.io.StringWriter;

public class XmlUtils {
    private static final SAXReader sax = new SAXReader();
    public static String formatXml(String xmlString) throws DocumentException, IOException {
        // 加载XML文档
        Document document = DocumentHelper.parseText(xmlString);

        // 创建格式化输出的OutputFormat对象
        OutputFormat format = OutputFormat.createPrettyPrint();
        format.setIndent("    "); //  缩进

        // 创建XMLWriter对象，输出到字符串缓冲区
        StringWriter sw = new StringWriter();
        XMLWriter writer = new XMLWriter(sw, format);
        writer.setEscapeText(false); // 禁用特殊字符的转义

        // 输出格式化后的XML文档到字符串缓冲区
        writer.write(document);
        writer.flush();
        writer.close();

        // 返回格式化后的XML字符串
        return sw.toString();
    }

    private XmlUtils() {}

}
