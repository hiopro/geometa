const dataImportUrl = "../dataImport"
const succeedCode = 0;
layui.use(['form', 'toast'], function () {
    let form = layui.form;
    let toast = layui.toast;
    let $ = layui.jquery;

    // radio 点击事件
    let $useUrl = $("#useUrl");
    let $useFile = $("#useFile");
    let $useText = $("#useText");
    form.on('radio', function (data) {
        console.log("on radio");
        console.log(data.elem); //得到radio原始DOM对象
        console.log(data.value); //被点击的radio的value值
        if (data.value === "url") {
            $useUrl.removeClass("layui-hide");
            $useFile.addClass("layui-hide");
            $useText.addClass("layui-hide");
        } else if (data.value === "file") {
            $useFile.removeClass("layui-hide");
            $useUrl.addClass("layui-hide");
            $useText.addClass("layui-hide");
        } else if (data.value === "text") {
            $useText.removeClass("layui-hide");
            $useUrl.addClass("layui-hide");
            $useFile.addClass("layui-hide");
        }
    });

    // 开关点击事件
    form.on('switch', function(data){
        console.log("on switch");
        console.log(data);
        let elem = data.elem;
        if (elem.checked) {
            elem.value = 1;
        } else {
            elem.value = 0;
        }
        console.log(elem.value);
    });

    // 表单重置
    $(document).ready(function() {
        $("#dataImportForm").on("reset", function(event) {
            // 显示点击 radio 触发输入方式的切换
            // 默认的点击重置按钮只会切换 radio 按钮
            // 下方的输入方式不会变化
            $("#defaultRadio").trigger("click");
        });
    });

    // 只有显示调用点击这回触发这里，所以暂时不能把
    // radio 的代码都写在这里
    $(document).ready(function() {
        $('input[type="radio"]').on('click', function() {
            console.log("on redio click");
            // 获取选中的值
            let selectedValue = $('input[name="gender"]:checked').val();
            console.log(selectedValue);
            // 处理选中的值
            if (selectedValue === undefined) {
                $useUrl.removeClass("layui-hide");
                $useFile.addClass("layui-hide");
                $useText.addClass("layui-hide");
            }
        });
    });

    // 表单提交事件
    $(document).on("submit", "#dataImportForm", function (e) {
        e.preventDefault();
        let formData = new FormData($(this)[0]);
        console.log("on submit")
        // 得到所属的标准
        let selectedOption = document.getElementById('subtype-select').selectedOptions[0];
        let standard =  selectedOption.parentNode.getAttribute('data-group-value');

        formData.append('standard', standard);
        // 打印 formData 中的所有值
        for (const [key, value] of formData.entries()) {
            console.log(key, value);
        }


        // 加载画面
        let loading = layer.load();
        $.ajax({
            url: dataImportUrl, // 文件上传处理的URL
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            success: function (result) {
                // 文件上传成功后的回调函数
                layer.close(loading);
                if (result.code === succeedCode) {
                    toast.success({title: 'Success', message: result.message, position: 'topCenter'});
                } else {
                    toast.error({title: 'Error', message: result.message, position: 'topCenter'});
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // 文件上传失败后的回调函数
                layer.close(loading);
                toast.error({title: 'Error', message: textStatus + ": " + errorThrown, position: 'topCenter'});
            }
        });
    });

    // 显示上传文件名
    showFileName();
});

function showFileName() {
    const fileInput = document.querySelector('#file');
    const uploadFileName = document.querySelector('#fileName');

    fileInput.addEventListener('change', () => {
        const fileName = fileInput.files[0].name;
        uploadFileName.value = fileName;
    });
}