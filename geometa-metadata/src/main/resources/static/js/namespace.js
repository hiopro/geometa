const listNsUrl = "../namespace/list/"
const listNsBuCidUrl = "../namespace/listByCid/"
const deleteNsUrl = "../namespace/delete/"
const addNsUrl = "../namespace/add/"
const updateNsUrl = "../namespace/update/"
const succeedCode = 0;
let currentCid = null;
layui.use(['table', 'form', 'jquery', 'common', 'toast'], function () {
    const table = layui.table;
    const form = layui.form;
    const $ = layui.jquery;
    const toast = layui.toast;
    let cols = [
        [
            {
                title: 'ID',
                field: 'id',
                align: 'center',
            },

            {
                title: 'Prefix',
                field: 'prefix',
                align: 'center',
            },
            {
                title: 'URI',
                field: 'uri',
                align: 'center'
            },

            {
                title: '操作',
                toolbar: '#ns-bar',
                align: 'center',
                width: 130,
                fixed: 'right'
            }
        ]
    ];

    table.render({
        elem: '#ns-table',
        url: listNsUrl,
        request: {
            pageName: 'current',//页码的参数名称，默认：page
            limitName: 'size' //每页数据量的参数名，默认：limit
        },
        response: {
            countName: 'total' //规定数据总数的字段名称，默认：count
        },
        page: true,
        cols: cols,
        skin: 'line',
        toolbar: '#ns-toolbar',
        defaultToolbar: [{
            title: '刷新',
            layEvent: 'refresh',
            icon: 'layui-icon-refresh',
        }, 'filter', 'print', 'exports']
    });

    // 查询表单提交
    form.on('submit(ns-query)', function (data) {
        console.log("in query");
        console.log(data);

        table.reload('ns-table', {
            url: listNsUrl,
            where: data.field
        })

        return false;
    });

    // 工具事件
    table.on('tool(ns-table)', function (obj) {
        if (obj.event === 'remove') {
            remove(obj);
        } else if (obj.event === 'edit') {
            edit(obj);
        }
    });

    // 删除数据
    function remove(obj) {
        console.log("in remove");
        console.log(obj);
        layer.confirm('确定要删除该数据', {
            icon: 3,
            title: '提示'
        }, function (index) {
            layer.close(index);
            let loading = layer.load();
            $.ajax({
                url: deleteNsUrl,
                dataType: 'json',
                data: {id: obj.data['id']},
                type: 'POST',
                success: function (result) {
                    layer.close(loading);
                    // 删除成功
                    if (result.code === succeedCode) {
                        layer.msg(result.message, {
                            icon: 1,
                            time: 500
                        }, function () {
                            obj.del();
                        });
                        obj.del();
                        layer.closeAll(); // 关闭页面
                        toast.success({title: 'Success', message: result.message, position: 'topCenter'});
                    } else {
                        // 删除失败
                        layer.msg(result.message, {
                            icon: 2,
                            time: 1000
                        });
                        //toast.error({title: 'Error', message: result.message, position: 'topCenter'});
                    }
                }
            })
        });
    }

    // 打开编辑表单
    function edit(obj) {
        console.log("in edit");
        console.log(obj);
        // 表单数据回填
        $("#nid").val(obj.data.id);
        $("#prefix").val(obj.data.prefix);
        $("#uri").val(obj.data.uri);
        layer.open({
            type: 1,
            title: '修改',
            shade: 0.1,
            area: ['500px', '260px'],
            content: $("#nsEdit")
        });
    }

    // 编辑表单提交
    form.on("submit(editSubmit)", function (data) {
        $.ajax({
            url: updateNsUrl,
            dataType: 'json',
            data: data.field,
            type: 'POST',
            success: function (result) {
                if (result.code === succeedCode) {
                    layer.closeAll(); // 关闭页面
                    toast.success({title: 'Success', message: result.message, position: 'topCenter'});
                    table.reload("ns-table");
                } else {
                    toast.error({title: 'Error', message: result.message, position: 'topCenter'});
                }
            }
        })
    })

    // 表头工具条
    table.on('toolbar(ns-table)', function (obj) {
        console.log(obj)
        if (obj.event === 'add') {
            add(obj);
        } else if (obj.event === 'refresh') {
            refresh();
        }
    });

    // 打开新增表单
    function add() {
        console.log("in add");
        let tableDate = table.getData("ns-table");
        console.log(tableDate)
        if (tableDate.length === 0) {
            toast.error({title: 'Error', message: "没有数据", position: 'topCenter'});
            return;
        }
        currentCid = tableDate[0].cid;
        layer.open({
            type: 1,
            title: '新增',
            shade: 0.1,
            area: ['500px', '260px'],
            content: $("#nsAdd")
        });
    }


    // 新增表单提交
    form.on("submit(addSubmit)", function (data) {
        console.log("on add submit")
        data.field.cid = currentCid;
        console.log(data.field)
        $.ajax({
            url: addNsUrl,
            dataType: 'json',
            data: data.field,
            type: 'POST',
            success: function (result) {
                if (result.code === succeedCode) {
                    layer.closeAll(); // 关闭页面
                    toast.success({title: 'Success', message: result.message, position: 'topCenter'});
                    table.reload("ns-table");
                } else {
                    toast.error({title: 'Error', message: result.message, position: 'topCenter'});
                }
            }
        })
    })

    function refresh() {
        let tableDate = table.getData("ns-table");
        if (tableDate.length === 0) {
            return;
        }
        currentCid = tableDate[0].cid
        table.reload('ns-table', {
            url: listNsBuCidUrl,
            where: {cid: currentCid}
        })
    }

})