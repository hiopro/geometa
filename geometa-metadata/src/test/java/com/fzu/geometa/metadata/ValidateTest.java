package com.fzu.geometa.metadata;

import cn.hutool.core.util.CharsetUtil;
import com.fzu.geometa.metadata.model.po.Item;
import com.fzu.geometa.metadata.service.ItemService;
import com.fzu.geometa.metadata.service.ItemValidateService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.nio.charset.StandardCharsets;

@SpringBootTest
public class ValidateTest {
    @Autowired
    ItemValidateService itemValidateService;

    @Test
    public void updateTest() {
        Item item = new Item();
        item.setEid(1653051836204531714L);
        item.setName("def");
        item.setPid(1647582956861431809L);
        item.setOrder(6);
        item.setCid(1647582957054369835L);
        item.setValue("abc");
        itemValidateService.updateWithValidation(item);
    }
}
