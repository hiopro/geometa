package com.fzu.geometa.metadata.eventdriven;

import com.fzu.geometa.metadata.eventdriven.event.MetadataAddEvent;
import com.fzu.geometa.metadata.eventdriven.publisher.MetadataEventPublisher;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class EventTest {
    @Autowired
    MetadataEventPublisher eventPublisher;

    @Test
    public void addTest() {
        MetadataAddEvent metadataAddEvent = new MetadataAddEvent(this, 1L);
        eventPublisher.publish(metadataAddEvent);
    }
}
