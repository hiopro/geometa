package com.fzu.geometa.metadata.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.fzu.geometa.metadata.model.po.Item;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ItemServiceTest {
    @Autowired
    ItemService itemService;

    @Test
    public void test() {
        LambdaQueryWrapper<Item> lqw = new LambdaQueryWrapper<>();
        lqw.eq(Item::getEid,1638465061498019841L);

        Item one = itemService.getOne(lqw);
        System.out.println(one);

    }
}
