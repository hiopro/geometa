package com.fzu.geometa.metadata.service;

import com.fzu.geometa.metadata.model.po.Item;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ItemValidateServiceTest {
    @Autowired
    ItemValidateService itemValidateService;

    @Test
    public void updateTest() {
        Item item = new Item();
        item.setEid(1653370397615648774L);
        // 非法标签
        item.setName("domainSet2");
        item.setPid(1653370397598871553L);
        item.setOrder(4);
        item.setCid(1653370397946998785L);
        item.setNamespace("gml");
        // item.setValue("abc");
        itemValidateService.updateWithValidation(item);
    }

    @Test
    public void addTest() {
        Item item = new Item();
        //  添加非法标签
        item.setName("def");
        item.setPid(1653370397598871553L);
        itemValidateService.addWithValidation(item);
    }

    @Test
    public void delTest() {
        // 删除必选标签
        itemValidateService.deleteWithValidation(1653370397615648774L);
    }

}
