package com.fzu.geometa.metadata.service;

import com.fzu.geometa.metadata.model.po.Register;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class RegisterServiceTest {
    @Autowired
    RegisterService registerService;

    @Test
    public void getByIdTest() {
        Register register = registerService.getById(1674625631615889410L);
        System.out.println(register);
    }

    @Test
    public void removeTest() {
        boolean res = registerService.removeById(1674625631615889410L);
        System.out.println(res);
    }

    @Test
    public void updateTest() {
        Register tt = registerService.getByCoverageId("tt");
        // 加载缓存
        registerService.getById(tt.getCid());
        tt.setDescription("abc");
        registerService.updateById(tt);
    }
}
