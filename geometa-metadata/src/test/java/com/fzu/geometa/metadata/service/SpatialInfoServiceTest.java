package com.fzu.geometa.metadata.service;

import com.fzu.geometa.metadata.model.po.SpatialInfo;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKTReader;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static com.fzu.geometa.metadata.mapper.SpatialInfoMapper.*;

@SpringBootTest
public class SpatialInfoServiceTest {
    @Autowired
    SpatialInfoService spatialInfoService;

    @Test
    public void saveTest() {
        SpatialInfo spatialInfo = new SpatialInfo();
        Coordinate[] coordinates = new Coordinate[] {
                new Coordinate(0, 0),
                new Coordinate(0, 10),
                new Coordinate(10, 10),
                new Coordinate(10, 0),
                new Coordinate(0, 0)
        };
        GeometryFactory geometryFactory = new GeometryFactory();
        Polygon polygon = geometryFactory.createPolygon(coordinates);
        spatialInfo.setCid(1L);
        spatialInfo.setEnvelope(polygon);
        spatialInfoService.save(spatialInfo);
    }

    @Test
    public void getTest() {
        List<SpatialInfo> list = spatialInfoService.list();
        for (SpatialInfo spatialInfo : list) {
            System.out.println(spatialInfo);
        }
    }

    @Test
    public void queryTest() throws ParseException {
        WKTReader wktReader = new WKTReader();
        Geometry geometry = wktReader.read("POLYGON((0 0, 10 0, 10 10, 0 10, 0 0))");
        System.out.println("input: ");
        System.out.println(geometry);

        System.out.println(RELATION_CONTAINS);
        List<SpatialInfo> spatialInfos = spatialInfoService.spatialInfoQuery((Polygon) geometry, RELATION_CONTAINS);
        for (SpatialInfo spatialInfo : spatialInfos) {
            System.out.println(spatialInfo);
        }

        System.out.println(RELATION_INTERSECTS);
        spatialInfos = spatialInfoService.spatialInfoQuery((Polygon) geometry, RELATION_INTERSECTS);
        for (SpatialInfo spatialInfo : spatialInfos) {
            System.out.println(spatialInfo);
        }

        System.out.println(RELATION_WITHIN);
        spatialInfos = spatialInfoService.spatialInfoQuery((Polygon) geometry, RELATION_WITHIN);
        for (SpatialInfo spatialInfo : spatialInfos) {
            System.out.println(spatialInfo);
        }

        System.out.println("other");
        spatialInfos = spatialInfoService.spatialInfoQuery((Polygon) geometry, "other");
        for (SpatialInfo spatialInfo : spatialInfos) {
            System.out.println(spatialInfo);
        }
    }

    @Test
    public void updateTest() throws ParseException {
        WKTReader wktReader = new WKTReader();
        Geometry geometry = wktReader.read("POLYGON((1 1, 0 15, 15 15, 15 0, 1 1))");
        SpatialInfo spatialInfo = new SpatialInfo();
        spatialInfo.setEnvelope((Polygon) geometry);
        spatialInfo.setId(555L);

        spatialInfoService.updateById(spatialInfo);

        spatialInfo.setCid(50L);
        spatialInfoService.updateById(spatialInfo);

    }
}
