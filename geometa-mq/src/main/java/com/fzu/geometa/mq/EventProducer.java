package com.fzu.geometa.mq;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fzu.geometa.mq.model.Event;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;

@Component
public class EventProducer {
    @Autowired
    KafkaTemplate kafkaTemplate;

    ObjectMapper objectMapper ;

    public EventProducer() {
        objectMapper = new ObjectMapper();
        objectMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
    }

    // 处理事件
    public void fireEvent(Event event) {
        // 将事件发布到指定主题
        try {
            kafkaTemplate.send(event.getTopic(),objectMapper.writeValueAsString(event));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
