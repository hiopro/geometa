package com.fzu.geometa.mq.constant;

public interface EntityType {
    // 元数据
    int Metadata = 0;
    // 消息
    int Message = 1;
}
