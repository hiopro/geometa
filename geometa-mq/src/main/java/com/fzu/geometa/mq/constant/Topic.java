package com.fzu.geometa.mq.constant;

public interface Topic {
    // 元数据发布
    String PUBLISH = "publish";
    // 系统通知
    String Notify = "notify";
    // 元数据撤销
    String RETRACT = "retract";
}
