package com.fzu.geometa.mq.model;

import lombok.Data;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

@Getter
public class Event {
    private String topic;
    private Long userId; // 发送者
    private Integer entityType; // 实体类型
    private Long entityId; // 实体 id
    private Map<Object,Object> data = new HashMap<>();

    public Event setTopic(String topic) {
        this.topic = topic;
        return this;
    }

    public Event setUserId(Long userId) {
        this.userId = userId;
        return this;
    }

    public Event setEntityType(Integer entityType) {
        this.entityType = entityType;
        return this;
    }

    public Event setEntityId(Long entityUserId) {
        this.entityId = entityUserId;
        return this;
    }

    public Event setData(String key, Object value) {
        this.data.put(key,value);
        return this;
    }

    public Object getData(String key) {
        return data.get(key);
    }
}
