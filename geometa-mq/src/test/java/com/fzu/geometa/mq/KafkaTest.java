package com.fzu.geometa.mq;

import org.apache.kafka.clients.consumer.ConsumerRecord;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
import org.springframework.test.context.TestPropertySource;

@SpringBootTest(classes = KafkaTest.class)
@TestPropertySource(value = "classpath:/application.yml")
@ComponentScan({ "com.fzu.geometa.mq"})
@EnableAutoConfiguration
public class KafkaTest {
    @Autowired
    KafkaProducer kafkaProducer;

    @Test
    public void testKafka() {
        kafkaProducer.sendMessage("my-test","你好");
        kafkaProducer.sendMessage("my-test","在吗");
        try {
            Thread.sleep(1000 * 10);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}

@Component
class KafkaProducer {
    @Autowired
    KafkaTemplate kafkaTemplate;
    public void sendMessage(String topic, String content) {
        kafkaTemplate.send(topic,content);
    }
}

@Component
class KafkaConsumer {
    @KafkaListener(topics = {"my-test"})
    public void handleMessage(ConsumerRecord record) {
        System.out.println(record.value());
    }
}

