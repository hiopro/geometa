package com.fzu.geometa.rpc;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.17.1)",
    comments = "Source: validate.proto")
public final class ValidateServiceGrpc {

  private ValidateServiceGrpc() {}

  public static final String SERVICE_NAME = "com.fzu.geometa.rpc.ValidateService";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<ValidateRequest,
      ValidateResponse> getValidateMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "validate",
      requestType = ValidateRequest.class,
      responseType = ValidateResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<ValidateRequest,
      ValidateResponse> getValidateMethod() {
    io.grpc.MethodDescriptor<ValidateRequest, ValidateResponse> getValidateMethod;
    if ((getValidateMethod = ValidateServiceGrpc.getValidateMethod) == null) {
      synchronized (ValidateServiceGrpc.class) {
        if ((getValidateMethod = ValidateServiceGrpc.getValidateMethod) == null) {
          ValidateServiceGrpc.getValidateMethod = getValidateMethod = 
              io.grpc.MethodDescriptor.<ValidateRequest, ValidateResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "com.fzu.geometa.rpc.ValidateService", "validate"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  ValidateRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  ValidateResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new ValidateServiceMethodDescriptorSupplier("validate"))
                  .build();
          }
        }
     }
     return getValidateMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static ValidateServiceStub newStub(io.grpc.Channel channel) {
    return new ValidateServiceStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static ValidateServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new ValidateServiceBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static ValidateServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new ValidateServiceFutureStub(channel);
  }

  /**
   */
  public static abstract class ValidateServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public void validate(ValidateRequest request,
                         io.grpc.stub.StreamObserver<ValidateResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getValidateMethod(), responseObserver);
    }

    @Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getValidateMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                ValidateRequest,
                ValidateResponse>(
                  this, METHODID_VALIDATE)))
          .build();
    }
  }

  /**
   */
  public static final class ValidateServiceStub extends io.grpc.stub.AbstractStub<ValidateServiceStub> {
    private ValidateServiceStub(io.grpc.Channel channel) {
      super(channel);
    }

    private ValidateServiceStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @Override
    protected ValidateServiceStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new ValidateServiceStub(channel, callOptions);
    }

    /**
     */
    public void validate(ValidateRequest request,
                         io.grpc.stub.StreamObserver<ValidateResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getValidateMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class ValidateServiceBlockingStub extends io.grpc.stub.AbstractStub<ValidateServiceBlockingStub> {
    private ValidateServiceBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private ValidateServiceBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @Override
    protected ValidateServiceBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new ValidateServiceBlockingStub(channel, callOptions);
    }

    /**
     */
    public ValidateResponse validate(ValidateRequest request) {
      return blockingUnaryCall(
          getChannel(), getValidateMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class ValidateServiceFutureStub extends io.grpc.stub.AbstractStub<ValidateServiceFutureStub> {
    private ValidateServiceFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private ValidateServiceFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @Override
    protected ValidateServiceFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new ValidateServiceFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<ValidateResponse> validate(
        ValidateRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getValidateMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_VALIDATE = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final ValidateServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(ValidateServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_VALIDATE:
          serviceImpl.validate((ValidateRequest) request,
              (io.grpc.stub.StreamObserver<ValidateResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @Override
    @SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class ValidateServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    ValidateServiceBaseDescriptorSupplier() {}

    @Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return Validate.getDescriptor();
    }

    @Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("ValidateService");
    }
  }

  private static final class ValidateServiceFileDescriptorSupplier
      extends ValidateServiceBaseDescriptorSupplier {
    ValidateServiceFileDescriptorSupplier() {}
  }

  private static final class ValidateServiceMethodDescriptorSupplier
      extends ValidateServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    ValidateServiceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (ValidateServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new ValidateServiceFileDescriptorSupplier())
              .addMethod(getValidateMethod())
              .build();
        }
      }
    }
    return result;
  }
}
