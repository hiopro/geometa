package com.fzu.geometa.search.dao;

import com.fzu.geometa.search.model.Metadata;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MetadataRepository extends ElasticsearchRepository<Metadata,Long> {
}
