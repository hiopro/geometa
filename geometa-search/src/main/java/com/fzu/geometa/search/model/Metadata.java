package com.fzu.geometa.search.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.Date;

@Document(indexName = "metadata")
@Data
public class Metadata {
    @Id
    private Long id;

    @Field(type = FieldType.Text)
    private String coverageId;
    @Field(type = FieldType.Text)
    private String description;
    @Field(type = FieldType.Text)
    private String subtype;
    @Field(type = FieldType.Date)
    private Date createTime;
    @Field(type = FieldType.Text)
    private String content;
}
