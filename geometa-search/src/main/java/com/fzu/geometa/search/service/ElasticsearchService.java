package com.fzu.geometa.search.service;

import com.fzu.geometa.search.dao.MetadataRepository;
import com.fzu.geometa.search.model.Metadata;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ElasticsearchService {
    @Autowired
    MetadataRepository metadataRepository;
    @Autowired
    private ElasticsearchOperations operations;

    // 新增及更新
    public void save(Metadata metadata) {
        metadataRepository.save(metadata);
    }

    public void deleteById(Long id) {
        metadataRepository.deleteById(id);
    }

    public Page<Metadata> searchMetadata(String keyword, int current, int limit) {
        Query searchQuery = new NativeSearchQueryBuilder()
                .withQuery(QueryBuilders.multiMatchQuery(keyword,  "coverageId","description","content")) // 查询条件
                .withSorts(SortBuilders.fieldSort("createTime").order(SortOrder.DESC)) // 排序
                .withPageable(PageRequest.of(current, limit)) // 分页
                .withHighlightFields(  // 高亮
                        new HighlightBuilder.Field("coverageId").preTags("<em>").postTags("</em>"),
                        new HighlightBuilder.Field("description").preTags("<em>").postTags("</em>"),
                        new HighlightBuilder.Field("content").preTags("<em>").postTags("</em>")
                ).build();
        SearchHits<Metadata> searchHits = operations.search(searchQuery, Metadata.class);
        List<Metadata> list = new ArrayList<>();
        for (SearchHit<Metadata> searchHit : searchHits.getSearchHits()) {
            Metadata metadata = searchHit.getContent();
            List<String> coverageIdField = searchHit.getHighlightFields().get("coverageId");
            if (coverageIdField != null) {
                metadata.setContent(coverageIdField.get(0));
            }
            List<String> descriptionField = searchHit.getHighlightFields().get("description");
            if (descriptionField != null) {
                metadata.setContent(descriptionField.get(0));
            }
            List<String> contentField = searchHit.getHighlightFields().get("content");
            if (contentField != null) {
                metadata.setContent(contentField.get(0));
            }
            list.add(metadata);
        }

        for (Metadata metadata : list) {
            System.out.println(metadata);
        }

        //  分页结果
        return new PageImpl<>(list, searchQuery.getPageable(), searchHits.getTotalHits());
    }
}
