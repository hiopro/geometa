package com.fzu.geometa.search;

import com.fzu.geometa.search.dao.MetadataRepository;
import com.fzu.geometa.search.model.Metadata;
import org.apache.lucene.util.QueryBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.MultiMatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.client.elc.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.client.elc.NativeQueryBuilder;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.Query;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@SpringBootTest
public class ElasticsearchTest {
    @Autowired
    MetadataRepository metadataRepository;

    @Autowired
    private ElasticsearchOperations operations;

    @Test
    public void testInsert() {
        Metadata metadata = new Metadata();
        metadata.setId(1L);
        metadata.setCoverageId("cov_test");
        metadata.setSubtype("abc");
        metadata.setContent("<a>hello world</a>");
        metadata.setCreateTime(new Date());
        metadataRepository.save(metadata);
    }

    @Test
    public void testInsertList() {
        Metadata metadata = new Metadata();
        metadata.setId(1L);
        metadata.setCoverageId("cov_test");
        metadata.setSubtype("abc");
        metadata.setContent("<a>hello world</a>");
        metadata.setCreateTime(new Date());
        Metadata metadata2 = new Metadata();
        metadata2.setId(2L);
        metadata2.setCoverageId("cov_test2");
        metadata2.setSubtype("abc");
        metadata2.setContent("<b>How are you?</b>");
        metadata2.setCreateTime(new Date());
        Metadata metadata3 = new Metadata();
        metadata2.setId(3L);
        metadata2.setCoverageId("cov_test3");
        metadata2.setSubtype("abc");
        metadata2.setContent("<c>I'm fine.</c>");
        metadata2.setCreateTime(new Date());
        List<Metadata> list = new ArrayList<>();
        list.add(metadata);
        list.add(metadata2);
        list.add(metadata3);
        metadataRepository.saveAll(list);
    }

    @Test
    public void testUpdate() {
        Metadata metadata = new Metadata();
        metadata.setId(1L);
        metadata.setCoverageId("cov_test_1");
        metadata.setSubtype("abcdef");
        metadata.setContent("<a>hello hello world!</a>");
        metadata.setCreateTime(new Date());
        metadataRepository.save(metadata);
    }

    @Test
    public void testDelete() {
        metadataRepository.deleteAll();
    }

    @Test
    public void testSearch() {
        Query searchQuery = new NativeSearchQueryBuilder()
                .withQuery(QueryBuilders.multiMatchQuery("hello", "coverageId", "content")) // 查询条件
                .withSorts(SortBuilders.fieldSort("createTime").order(SortOrder.DESC)) // 排序
                .withPageable(PageRequest.of(0, 10)) // 分页
                .withHighlightFields(  // 高亮
                        new HighlightBuilder.Field("coverageId").preTags("<em>").postTags("</em>"),
                        new HighlightBuilder.Field("content").preTags("<em>").postTags("</em>")
                ).build();
        SearchHits<Metadata> searchHits = operations.search(searchQuery, Metadata.class);
        List<Metadata> list = new ArrayList<>();
        for (SearchHit<Metadata> searchHit : searchHits.getSearchHits()) {
            Metadata metadata = searchHit.getContent();
            String coverageId = metadata.getCoverageId();
            List<String> coverageIdField = searchHit.getHighlightFields().get("coverage");
            if (coverageIdField != null) {
                metadata.setContent(coverageIdField.get(0));
            }
            List<String> contentField = searchHit.getHighlightFields().get("content");
            if (contentField != null) {
                metadata.setContent(contentField.get(0));
            }
            list.add(metadata);
        }

        for (Metadata metadata : list) {
            System.out.println(metadata);
        }

        //  分页结果
        Page<Metadata> metadataPage = new PageImpl<>(list, searchQuery.getPageable(), searchHits.getTotalHits());
    }
}
