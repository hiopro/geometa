package com.fzu.geometa;

import com.fzu.geometa.validate.ValidateRpcServer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;

@SpringBootApplication
public class ValidateApplication {

    @Value("${rpc.port}")
    static int port;
    public static void main(String[] args) {
        SpringApplication.run(ValidateApplication.class, args);
        ValidateRpcServer validateRpcServer = new ValidateRpcServer(8085); // port 注入不进
        try {
            validateRpcServer.start();
            validateRpcServer.blockUntilShutdown();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

}
