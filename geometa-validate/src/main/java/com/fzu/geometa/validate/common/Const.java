package com.fzu.geometa.validate.common;

import com.fzu.geometa.validate.test.ISO19123Validation;
import com.fzu.geometa.validate.test.ISO19163Validation;

public interface Const {
    String ISO_19163_TEST_CLASS = "com.fzu.geometa.validate.test.ISO19163Test";
    Class ISO_19163_VALIDATION_CLASS = ISO19163Validation.class;
    Class ISO_19123_VALIDATION_CLASS = ISO19123Validation.class;
    String TEST_LISTENER_CLASS = "com.fzu.geometa.validate.listener.MyTestListener";
    String EXECUTION_LISTENER_CLASS = "com.fzu.geometa.validate.listener.MyExecutionListener";
    String REPORTER_CLASS = "org.uncommons.reportng.HTMLReporter";
    String VALIDATE_LISTENER_CLASS = "com.fzu.geometa.validate.listener.ValidateListener";
}
