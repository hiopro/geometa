package com.fzu.geometa.validate.common;

public interface Standard {
    String ISO_19163 = "ISO_19163";
    String ISO_19123 = "ISO_19123";
}