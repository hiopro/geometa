package com.fzu.geometa.validate.config;

import cn.hutool.core.io.resource.ResourceUtil;
import com.fzu.geometa.validate.common.Standard;
import com.fzu.geometa.validate.service.ValidateServiceImpl;
import com.fzu.geometa.validate.test.DataType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import static com.fzu.geometa.validate.test.DataType.cat;

/**
 * 加载 schema
 */
@Component
@Slf4j
public class SchemaInitializer implements CommandLineRunner {
    @Autowired
    ValidateServiceImpl validateService;

    @Override
    public void run(String... args) throws Exception {
        log.info("加载资源并测试系统功能");
        String xmlStr = ResourceUtil.readUtf8Str("init/cat.xml");
        validateService.validate(Standard.ISO_19163, cat, xmlStr);
        xmlStr = ResourceUtil.readUtf8Str("init/exampleRectifiedGridCoverage-2.xml");
        validateService.validate(Standard.ISO_19123, DataType.rectified, xmlStr);
        log.info("测试完成");
    }
}
