package com.fzu.geometa.validate.controller;

import cn.hutool.core.date.DateUtil;
import com.fzu.geometa.validate.service.TestExecService;
import com.fzu.geometa.validate.service.TestRecordService;
import com.fzu.geometa.validate.dao.TestParameters;
import com.fzu.geometa.validate.dao.TestRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.io.IOException;
import java.util.List;

@Controller
public class MainController {

    @Autowired
    TestExecService testExecService;

    @Autowired
    TestRecordService testRecordService;

    @GetMapping("/index")
    public String index(Model model) throws IOException {
        List<TestRecord> records = testRecordService.getAll();
        model.addAttribute("records", records);
        return "index";
    }

    @GetMapping("/test/create")
    public String createTest() {
        return "test";
    }

    @PostMapping("/test/exec")
    public String execTest(TestParameters param) throws IOException {

        TestRecord tr = new TestRecord();
        tr.setTime(DateUtil.now());
        tr.setTestName(param.getTestName());
        tr.setDataType(param.getDataType());
        testRecordService.save(tr);

        testExecService.ExecuteTest(param, tr.getId());

        return "redirect:/test/result/" + tr.getId() + "/html/index.html";
    }

    @GetMapping("/test/del")
    public String delRecord(String id) {
        testRecordService.del(id);
        return "redirect:/index";
    }
}
