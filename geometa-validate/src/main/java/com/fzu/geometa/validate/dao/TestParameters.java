package com.fzu.geometa.validate.dao;

import lombok.Data;

@Data
public class TestParameters {
    private String testName;
    private String testUrl;
    private String dataType;
}
