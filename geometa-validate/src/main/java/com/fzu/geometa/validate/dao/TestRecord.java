package com.fzu.geometa.validate.dao;

import lombok.Data;

@Data
public class TestRecord {
    String id;
    String dataType;
    String testName;
    String time;
}
