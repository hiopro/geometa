package com.fzu.geometa.validate.dao;

import lombok.Data;

@Data
public class XPathCase {
    private int id;
    private String xpath;
    private String expect;
}
