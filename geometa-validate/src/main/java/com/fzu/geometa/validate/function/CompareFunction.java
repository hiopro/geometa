package com.fzu.geometa.validate.function;

import org.jaxen.Context;
import org.jaxen.Function;
import org.jaxen.FunctionCallException;

import java.util.List;

/**
 *  fn:compare(comp1,comp2)
 *  如果 comp1 小于 comp2，则返回 -1。如果 comp1 等于 comp2，则返回 0。
 *  如果 comp1 大于 comp2，则返回 1。（根据所用的对照规则）。
 *  这里传入的参数定义为字符串
 */
public class CompareFunction  implements Function {
    @Override
    public Object call(Context context, List args) throws FunctionCallException {
        if (args.size() != 2) {
            throw new FunctionCallException("只能传入两个参数");
        }
        Object o1 = args.get(0);
        Object o2 = args.get(1);
        if (!(o1 instanceof String && o2 instanceof String)) {
            throw new FunctionCallException("暂不支持这种类型的参数");
        }
        String arg1 = (String) o1;
        String arg2 = (String) o2;
        return arg1.compareTo(arg2);
    }
}
