package com.fzu.geometa.validate.function;

import org.dom4j.Attribute;
import org.jaxen.Context;
import org.jaxen.Function;
import org.jaxen.FunctionCallException;

import java.util.List;

/**
 * fn:deep-equal(param1,param2,collation)
 * 如果 param1 和 param2 与彼此相等（deep-equal），则返回 true，否则返回 false。
 * 这里传入的参数定义为经过xpath选择后的属性
 */
public class DeepEqualFunction implements Function {
    @Override
    public Object call(Context context, List args) throws FunctionCallException {
        if (args.size() != 2) {
            throw new FunctionCallException("只能传入两个参数");
        }

        Object obj1 = args.get(0);
        Object obj2 = args.get(1);

        if (!(obj1 instanceof List && obj2 instanceof List)) {
            throw new FunctionCallException("暂不支持这种类型的参数");
        }

        List arg1 = (List)obj1;
        List arg2 = (List)obj2;

        for (Object o1 : arg1) {
            if (!(o1 instanceof Attribute)) {
                throw new FunctionCallException("暂不支持这种类型的参数");
            }
            Attribute a1 = (Attribute) o1;
            for (Object o2 : arg2) {
                if (!(o2 instanceof Attribute)) {
                    throw new FunctionCallException("暂不支持这种类型的参数");
                }
                Attribute a2 = (Attribute) o2;
                // 属性名与属性值要都相等
                if (!a1.getQualifiedName().equals(a2.getQualifiedName())
                        || !a1.getValue().equals(a2.getValue())) {
                    return false;
                }
            }
        }
        return true;
    }
}
