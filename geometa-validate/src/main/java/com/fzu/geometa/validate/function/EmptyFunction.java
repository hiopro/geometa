package com.fzu.geometa.validate.function;

import org.jaxen.Context;
import org.jaxen.Function;
import org.jaxen.FunctionCallException;

import java.util.List;

/**
 * fn:empty(item,item,...)
 * 如果参数值是空序列，则返回 true，否则返回 false。
 * 这里传入的参数定义为为经过xpath选择后的节点
 */
public class EmptyFunction implements Function {

    @Override
    public Object call(Context context, List args) throws FunctionCallException {
        if (args.isEmpty()) {
            throw new FunctionCallException("参数不能为空");
        }
        for (Object arg : args) {
            if (!(arg instanceof List)) {
                throw new FunctionCallException("暂不支持这种类型的参数");
            }
            // 节点集合不为空就返回false
            List list = (List) arg;
            if (!list.isEmpty()) {
                return false;
            }
        }
        return true;
    }
}
