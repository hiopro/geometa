package com.fzu.geometa.validate.listener;

import org.testng.IExecutionListener;
import org.testng.Reporter;

public class MyExecutionListener implements IExecutionListener {

    @Override
    public void onExecutionStart() {
        // 防止日志积累
        Reporter.clear();
    }

    @Override
    public void onExecutionFinish() {
        // 防止日志积累
        Reporter.clear();
    }
}
