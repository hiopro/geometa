package com.fzu.geometa.validate.listener;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.Reporter;

public class MyTestListener implements ITestListener {
    @Override
    public void onStart(ITestContext context) {
        Reporter.log("Test name: " + context.getName());
        // 只运行了一个组
        String group = context.getIncludedGroups()[0];
        Reporter.log("Executed group: " + group);
    }
}
