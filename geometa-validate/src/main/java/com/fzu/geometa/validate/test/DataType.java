package com.fzu.geometa.validate.test;

public interface DataType {
    // ISO 19163
    String cat = "CategoricalGriddedData";
    String num = "NumericalGriddedData";
    String opt = "OpticalImage";
    String sar = "SARData";
    String rad = "RadiometerData";
    String fus = "FusedImage";
    String sim = "SimulatedImage";

    // IOS 19123
    String rectified = "RectifiedGridCoverage";
    String referenceable = "ReferenceableGridCoverage";
}
