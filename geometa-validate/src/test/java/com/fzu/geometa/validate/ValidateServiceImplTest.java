package com.fzu.geometa.validate;

import cn.hutool.core.io.resource.ResourceUtil;
import com.fzu.geometa.validate.common.Standard;
import com.fzu.geometa.validate.service.ValidateServiceImpl;
import com.fzu.geometa.validate.test.DataType;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static com.fzu.geometa.validate.test.DataType.cat;

@SpringBootTest
public class ValidateServiceImplTest {
    @Autowired
    ValidateServiceImpl validateServiceImpl;

    @Test
    public void test() {
        String xmlStr = ResourceUtil.readUtf8Str("static/case/cat.xml");
        boolean res = validateServiceImpl.validate("ISO19163", cat, xmlStr);
        System.out.println(res);
    }

    @Test
    public void test2() {
        String xmlStr = ResourceUtil.readUtf8Str("static/case/cat-2.xml");
        boolean res = validateServiceImpl.validate("ISO19163", cat, xmlStr);
        System.out.println(res);
    }

    @Test
    public void test9123() {
        String xmlStr = ResourceUtil.readUtf8Str("ets_wcs20_resources/gmlcov/1.0/examples/exampleRectifiedGridCoverage-1.xml");
        boolean res = validateServiceImpl.validate(Standard.ISO_19123, DataType.rectified,xmlStr);
        System.out.println(res);
    }
}
