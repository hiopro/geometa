package com.fzu.geometa.validate;

import cn.hutool.core.io.resource.ResourceUtil;
import com.fzu.geometa.validate.function.CompareFunction;
import com.fzu.geometa.validate.function.DeepEqualFunction;
import com.fzu.geometa.validate.function.EmptyFunction;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.XPath;
import org.dom4j.io.SAXReader;
import org.jaxen.FunctionContext;
import org.jaxen.XPathFunctionContext;
import org.junit.jupiter.api.Test;

import java.net.URL;

public class XpathTest {
    @Test
    public void test() throws DocumentException {

        FunctionContext instance = XPathFunctionContext.getInstance();
        XPathFunctionContext fc = (XPathFunctionContext) instance;
        fc.registerFunction(null,"empty",new EmptyFunction());
        fc.registerFunction(null,"deep-equal",new DeepEqualFunction());
        fc.registerFunction(null,"compare",new CompareFunction());

        SAXReader reader = new SAXReader();
        URL url = ResourceUtil.getResource("testcase/CategoricalGriddedData.xml");
        Document document = reader.read(url);
        XPath xPath = document.createXPath("empty(//*[local-name()='IE_SARData'])");

        System.out.println(xPath.getText());

        Object res = xPath.evaluate(document);
        System.out.println(res);
    }
}
