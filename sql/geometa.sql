/*
 Navicat Premium Data Transfer

 Source Server         : 本地MySQL
 Source Server Type    : MySQL
 Source Server Version : 80033
 Source Host           : localhost:3306
 Source Schema         : geometa

 Target Server Type    : MySQL
 Target Server Version : 80033
 File Encoding         : 65001

 Date: 24/06/2023 21:02:06
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for item
-- ----------------------------
DROP TABLE IF EXISTS `item`;
CREATE TABLE `item`  (
  `eid` bigint NOT NULL COMMENT '元数据项唯一标识',
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '元数据项名称',
  `pid` bigint NOT NULL COMMENT '父元数据项标识',
  `order` smallint NULL DEFAULT NULL COMMENT '在父元数据项中出现的次序',
  `namespace` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '命名空间',
  `attributes` json NULL COMMENT '属性',
  `value` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '元数据项的值',
  `cid` bigint NOT NULL COMMENT '所属的 Coverage',
  PRIMARY KEY (`eid`) USING BTREE,
  INDEX `cid_index`(`cid` ASC) USING BTREE COMMENT 'cid 索引',
  INDEX `pid_index`(`pid` ASC) USING BTREE COMMENT 'pid 索引'
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '元数据项表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for namespace
-- ----------------------------
DROP TABLE IF EXISTS `namespace`;
CREATE TABLE `namespace`  (
  `id` bigint NOT NULL COMMENT '唯一标识',
  `prefix` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '前缀名称',
  `uri` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'uri标识',
  `cid` bigint NOT NULL COMMENT '所属的coverage',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `cid_index`(`cid` ASC) USING BTREE COMMENT 'cid 索引'
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '命名空间表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for register
-- ----------------------------
DROP TABLE IF EXISTS `register`;
CREATE TABLE `register`  (
  `cid` bigint NOT NULL COMMENT 'Coverage 唯一标识',
  `coverage_id` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '字符形式的 Coverage 唯一标识',
  `description` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '描述信息',
  `subtype` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '子类型',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `user_id` bigint NULL DEFAULT NULL COMMENT '创建者',
  `standard` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '元数据的标准',
  `validate` tinyint NULL DEFAULT 0 COMMENT '是否开启验证 0 关闭 1 开启',
  PRIMARY KEY (`cid`) USING BTREE,
  UNIQUE INDEX `unique_coverage_id`(`coverage_id` ASC) USING BTREE COMMENT 'coverage_id 唯一'
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '注册表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for spatial_info
-- ----------------------------
DROP TABLE IF EXISTS `spatial_info`;
CREATE TABLE `spatial_info`  (
  `id` bigint NOT NULL COMMENT 'id',
  `envelope` polygon NOT NULL COMMENT '空间范围',
  `cid` bigint NOT NULL COMMENT '元数据 id',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `cid_index`(`cid` ASC) USING BTREE COMMENT '元数据 id 唯一索引',
  SPATIAL INDEX `envelope_index`(`envelope`) COMMENT '空间索引'
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `password` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `email` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `role` int NULL DEFAULT NULL COMMENT '0-普通用户; 1-超级管理员; 2-版主;',
  `status` int NULL DEFAULT NULL COMMENT '0-未激活; 1-已激活;',
  `activation_code` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `header_url` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT NULL,
  `salt` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_username`(`username`(20) ASC) USING BTREE,
  INDEX `index_email`(`email`(20) ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 152 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
